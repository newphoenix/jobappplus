<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Confirmation</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="/css/images/f.ico">
<link rel="stylesheet" type="text/css" href="/css/job.css"/>
</head>
<body>
<div id="main">
<div id="header">
<img src="/js/images/d.png" width="159px" height="52px" style="float:left" />
  <span id="langJS" style="float: right"> <a href="?lang=sr" style="color:#7C41A3">Bosanski</a>		
  | <a href="?lang=en" style="color:#7C41A3">English</a>						
  | <a href="/app/"><img src="/css/images/home.png" width="20px" height="20px" style="vertical-align: bottom;"></a> 
			</span>
</div>
<div id="content" style="text-align:center; height:50px">
   <spring:message code="confirmRegistration.message" text="Registered successfully" /> , <a href="/app/login.jsp"><spring:message code="confirmRegistration.login" text="Login page" /></a>
</div>
<div id="footer"><div> <%@ include file="footer" %> </div></div> 
</div>

</body>
</html>