<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Messages</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="/css/images/f.ico">
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="/js/quickpager.jquery.js"></script>
<script type="text/javascript" src="/js/commonStaff.js"></script>
<script type="text/javascript" src="/js/dropdown_simple.js"></script>
<script type="text/javascript" src="/js/jquery.i18n.properties-min-1.0.9.js"></script>
<script>

$(document).ready(	function() {	 
	showHideAdSection();
});
</script>
</head>
<body>

<div id="main">
<div id="header">
<img src="/js/images/d.png" width="159px" height="52px" style="float:left" />
   <div class="loginAndRegister">
   <span id="langJS" style="float: right">
     <a href="?f=0&lang=sr" style="color:#7C41A3">Bosanski</a>
    |<a href="?f=0&lang=en" style="color:#7C41A3">English</a>    
</span> 
     <%@ include file="photoheaderdata" %> 
    
   </div>
</div>
<div id="adSection" class="hide"></div>
<div id="message"></div>
<div id="content">
<div id="msgDialog" title=" " style="display:none"><p></p></div>

<div id="left">
  <div id="sidebar"><%@ include file="sidebar" %></div>
</div>

<div id="left-75">
<div id="jsErrorSection" class="errorDiv"></div>


 <table><tr>        
        <form id="getMessages" action="/web/Contact/all" method="post">
        <td><input type="checkbox" name="cb" id="or"><spring:message code="msgAll.unreadcb" text="Only Unread" />
            <input type="submit" id="getBtn" class="searchButton" value='<spring:message code="msgAll.get" text="Get Messages" />'></td>
		 
		</form>
		</tr>
		<tr><td>
		<div id="msgList">		
			<ul class="paging">			
				<c:forEach items="${msgLst}" var="msg" varStatus="status">
					<table class="table21" id="${msg.id}">					
						<tr>
							<td><spring:message code="msgAll.from" text="From: " /></td>						
							<td>${msg.email}</td>														
						</tr>
						<tr>
						<td><spring:message code="msgAll.subject" text="Subject: " /></td>
						<td>${msg.subject}</td>														
						</tr>
						<tr>
						<td><spring:message code="msgAll.created" text="Created: " /></td>						
						<td>${msg.created}</td>
						
						<td><a class="rms" m="${msg.id}"><c:if test = "${msg.read == '0'}"><span class="required">* </span></c:if><spring:message code="msgAll.show" text="Show" /></a></td>														
						</tr>
					</table>
				</c:forEach>
			</ul>			
		</div>
		</td></tr>
</table>

</div>


<div id="sA" class="flora" style="display:none" title="<spring:message code="com.t" text="Result"/>">
<table> <tr><td></td><td><label id="_d"></label></td></tr>
		<tr><td><b><spring:message code="msgAll.from" text="From:"/></b></td>
		    <td><label id="_e"></label></td></tr>		
		<tr><td><b><spring:message code="msgAll.subject" text="Subject:"/></b></td>
		<td><label id="_s"></label></td></tr>
		<tr><td></td><td><textarea rows="10" cols="60" id="_b"></textarea></td></tr>
		</table>
</div>



<input type="hidden" id="showAdSection" value="<c:out value="${showAdSection}" />"/>
<input type="hidden" value="${pageContext.response.locale}" id="userLanguage"/>
</div>
<div id="footer"><div> <%@ include file="footer" %> </div></div>	



<script>
$(document).ready(	function() {
	pager();
});
</script>
<script type="text/javascript" src="/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="/js/contactMsg.js"></script>

</div>
<script>
(function(e,t,n,r,i,s,o){e["GoogleAnalyticsObject"]=i;e[i]=e[i]||function(){(e[i].q=e[i].q||[]).push(arguments)},e[i].l=1*new Date;s=t.createElement(n),o=t.getElementsByTagName(n)[0];s.async=1;s.src=r;o.parentNode.insertBefore(s,o)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create","UA-43586413-1","automobily.ba");ga("send","pageview")
</script>

</body>
</html>