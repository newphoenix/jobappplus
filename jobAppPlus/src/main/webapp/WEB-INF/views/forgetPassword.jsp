<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Forget Password</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="/css/images/f.ico">
<link rel="stylesheet" type="text/css" href="/css/job.css"/>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="/js/jquery.i18n.properties-min-1.0.9.js"></script>
<script type="text/javascript" src="/js/commonStaff.js"></script>
</head>
<body>
	<div id="main">
	<div id="header">
	<img src="/js/images/d.png" width="159px" height="52px" style="float:left" />
	     <span id="langJS" style="float: right"> <a href="?lang=sr" style="color:#7C41A3">Bosanski</a>
	     |<a href="?lang=en" style="color:#7C41A3">English</a> 				
		 | <a href="/app/"><img src="/css/images/home.png" width="20px" height="20px" style="vertical-align: bottom;"></a> 
			</span>
	</div>
<div id="content">
	<form:form commandName="forgetPassword" action="/app/fpp/" onsubmit="return validateEmail();">
		<table class="forgetPasswordBox">
			<tr>
			   <td></td>
				<td colspan=2><form:errors path="*" class="springErrorBlock" />
				</td>				
			</tr>			
			<tr>
				<td colspan="2"><spring:message code="forgetPassword.email" text="Email Address :" /></td>
				<td><form:input path="email" size="60" />
				</td>				
			</tr>			
			<tr>
			<td></td>
			<td></td>		
				<td><input type="submit" class="btn" style="float:right" value="<spring:message code="forgetPassword.sendButton" text="Send" />" />
				</td>				
			</tr>
		</table>
	</form:form>

</div>

<input type="hidden" value="${pageContext.response.locale}" id="userLanguage"/>
<div id="footer"><div> <%@ include file="footer" %> </div></div> 
</div>	
</body>
</html>