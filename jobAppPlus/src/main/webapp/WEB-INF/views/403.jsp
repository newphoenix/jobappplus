<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<link rel="shortcut icon" href="/css/images/f.ico">
<link rel="stylesheet" type="text/css" href="/css/job.css"/>
</head>
<body>
<div id="main">
	<div id="header">
	<img src="/js/images/x.png" width="159px" height="52px" style="float:left" />
	</div>
<div id="adSection" class="hide"></div>
<div id="content" style="text-align: center;">
<h1><spring:message code="e403.msg" text="You do not have sufficient permissions to access this page." /></h1>
<input type="button" class="Btn" value="<spring:message code="errorpage.backButton" text="Back" />" onclick="history.back()"> 
</div></div>

<input type="hidden" value="${pageContext.response.locale}" id="userLanguage"/>
<div id="footer"></div> 
</body>
</html>