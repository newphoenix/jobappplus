<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<html style="background-color: #DAD7D7;">
<head>
<title>Contact</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="/css/images/f.ico">
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="/js/commonStaff.js"></script>
<script type="text/javascript" src="/js/dropdown_simple.js"></script>
<script type="text/javascript" src="/js/jquery.i18n.properties-min-1.0.9.js"></script>
<script>

$(document).ready(	function() { 
	 showHideAdSection();
});
</script>
</head>
<body>

<div id="main">
<div id="header">
<img src="/js/images/d.png" width="159px" height="52px" style="float:left" />
   <div class="loginAndRegister">
     <%@ include file="headerdata" %>  
  </div>
</div>
<div id="adSection" class="hide"></div>
<div id="message"></div>
<div id="content">
<div id="msgDialog" title=" " style="display:none"><p></p></div>

<div id="left">
  <div id="sidebar"><%@ include file="sidebar" %></div>
</div>

<div id="left-75">
<div id="jsErrorSection" class="errorDiv"></div>

<table>
    <tr>
        <td colspan="3"><spring:message code="contact.title" text="Contact" /></td>
    </tr>
    <tr>
        <td><spring:message code="contact.email" text="Email Address" /><span class="required">*</span></td>
        <td colspan="2"><input type="text" id="email" size="60" /></td>        
    </tr>
    <tr>
        <td><spring:message code="contact.subject" text="Subject" /><span class="required">*</span></td>
        <td colspan="2"><input type="text" id="sub" size="60" /></td>        
    </tr>
    <tr>
        <td><spring:message code="contact.msg" text="Message" /><span class="required">*</span></td>
        <td colspan="2"><textarea rows="8" cols="60" id="msg"></textarea></td>        
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td><input type="button" id="send" class="saveButton" value="<spring:message code="contact.send" text="send" />"  style="position:relative; top:25px; left: 50%;"></td>
    </tr>
</table>

</div>
<input type="hidden" id="showAdSection" value="<c:out value="${showAdSection}" />"/>
<input type="hidden" value="${pageContext.response.locale}" id="userLanguage"/>
</div>
<div id="footer"><div> <%@ include file="footer" %> </div></div>	
</div>

<script type="text/javascript" src="/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="/js/contact.js"></script>
</body>
</html>