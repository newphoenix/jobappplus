<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Reset Password</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="/css/images/f.ico">
<link rel="stylesheet" type="text/css" href="/css/job.css"/>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="/js/sha256.js"></script>
<script type="text/javascript" src="/js/commonStaff.js"></script>

<script type="text/javascript">
function processData() {	
	if ($("#newPassword").val().length < 9) {
		 writeMessage($.i18n.prop('rErr.password'));
		 return false;}

	if ($("#newPassword").val() != $("#confirmNewPassword").val()) {
		 writeMessage($.i18n.prop('rErr.confirmPassword'));
		 return false;}
	
	var hash1 = sha256_digest($("#newPassword").val());
	$("#newPassword").val(hash1);
	
	var hash2 = sha256_digest($("#confirmNewPassword").val());
	$("#confirmNewPassword").val(hash2);
  
    return true;	
}
</script>
</head>
<body>
<div id="main">
<div id="header">
<img src="/js/images/carlogono.png" width="159px" height="52px" style="float:left" />
    <span id="langJS" style="float: right"> <a href="reset?code=<c:out value="${carCode}" />&lang=sr" style="color:#7C41A3">Bosanski</a>
    | <a href="reset?code=<c:out value="${carCode}" />&lang=en" style="color:#7C41A3">English</a>
	| <a href="/app/"><img src="/css/images/home.png" width="20px" height="20px" style="vertical-align: bottom;"></a> 
			</span>
</div>
<div id="content" style="text-align:center; height:175px">
<div id="msgDialog" title=" " style="display:none"><p></p></div>

	<form:form commandName="resetPassword" action="/app/fpp/reset" onsubmit="return processData();">
		<table class="resetPasswordBox">
			<tr>
				<td colspan="2"><form:errors path="*" class="springErrorBlock2" />
				</td>
			</tr>
			<tr>
				<td><spring:message code="resetPassword.newPassword" text="New Password" /></td>
				<td><form:password path="newPassword" size="64" id="newPassword" />
				</td>				
			</tr>	
			<tr>
				<td><spring:message code="resetPassword.confirmPassword" text="Confirm Password" /></td>
				<td><form:password path="confirmNewPassword" size="64" id="confirmNewPassword" />
				</td>				
			</tr>		
			<tr>  
				<td colspan="2"><input type="submit" class="btn" value="<spring:message code="resetPassword.sendButton" text="Send" />" />
				</td>
			</tr>
		</table>
	</form:form>
</div>

<input type="hidden" value="${pageContext.response.locale}" id="userLanguage"/>
<div id="footer"><div> <%@ include file="footer" %> </div></div> 
</div>

</body>
</html>