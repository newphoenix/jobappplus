<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>about</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="/css/images/f.ico">
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="/js/commonStaff.js"></script>
<script type="text/javascript" src="/js/dropdown_simple.js"></script>
<script type="text/javascript" src="/js/jquery.i18n.properties-min-1.0.9.js"></script>
<script type="text/javascript">
$(document).ready(	function() {	
	showHideAdSection();
});
</script>	
</head>
<body>

<div id="main">
<div id="header">
<img src="/js/images/d.png" width="159px" height="52px" style="float:left" />
   <div class="loginAndRegister">
     <%@ include file="headerdata" %>  
  </div>
</div>
<div id="adSection" class="hide"></div>
<div id="message"></div>
<div id="content">
<div id="msgDialog" title=" " style="display:none"><p></p></div>

<div id="left">
  <div id="sidebar"><%@ include file="sidebar" %></div>
</div>

<div id="left-75">

<c:choose>
    <c:when test="${pageContext.response.locale != 'sr'}">
        Coming soon
    </c:when>
    <c:otherwise>
        Uskoro
    </c:otherwise>
</c:choose>

</div>
</div>

<input type="hidden" value="${pageContext.response.locale}" id="userLanguage"/>
<div id="footer"><div> <%@ include file="footer" %> </div></div>	
</div>
</body>
</html>