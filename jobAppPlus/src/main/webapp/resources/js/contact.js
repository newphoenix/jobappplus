$(document).ready(function() {

	
	$('#send').on("click",function (event) {
		
		if(!validateEmail()){w(); return;}
		
		var n = $('#sub').val();
		if(n == null || n.length == 0){w(); return;}
		
		var h = $('#msg').val();
        if(h == null || h.length == 0){w(); return;}
		
		$.blockUI({ message: '<h1><img src="/js/images/loading.gif" width="100px" height="15px" />'+$.i18n.prop('loadingMsg')+'</h1>' }); 
				
			$.ajax({
				type : "POST",
				url : "/app/Contact/msg",
				data : "e="+$('#email').val()+"&s="+$('#sub').val()+"&m="+$('#msg').val(),
				dataType:"json",
				success : function(data) {				
					if(data == 'ok'){
						$.unblockUI();
						messageBox($.i18n.prop('success'),$.i18n.prop('contact.msg'));
						$('#msg').val("");
						$('#sub').val("");
						$('#email').val("");					
					}else{
						messageBox($.i18n.prop('err'),$.i18n.prop('contact.nokmsg'));
					} 
				},
				error : function() {
					$.unblockUI();
					messageBox($.i18n.prop('err'),$.i18n.prop('errorAjaxCall'));
				}
			});			
			
		});	
	
});


function w(){
	messageBox($.i18n.prop('warning'),$.i18n.prop('conx.msg'));
}
