var appMessages = ['info','warning','error','success'];

function hideAllMessages()
{
		 var messagesHeights = new Array();
	 
		 for (var i=0; i<appMessages.length; i++)
		 {
				  messagesHeights[i] = $('.' + appMessages[i]).outerHeight();
				  $('.' + appMessages[i]).css('top', -messagesHeights[i]);  
		 }
}


$(document).ready(function(){
		
		 hideAllMessages();
		
		 $('.message').click(function(){			  
				  $(this).animate({top: -$(this).outerHeight()}, 500);
	     });		 
		
}); 