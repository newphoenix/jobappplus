$(document).ready(function(){
	//initI18n();	
	buildMsgBox();
});

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function is_int(value) {		
	var a = /^\d+$/;	
	return a.test(value);
}

function is_phone(value) {		
	var a = /^\d{3}\/\d{3}-\d{3,4}$/;	
	return a.test(value);
}

function isValidDate(dateString){  
    if(!/^\d{2}\/\d{2}\/\d{4}$/.test(dateString))
        return false;

    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);
  
    if(year < 1912 || year > (new Date().getFullYear()) || month == 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

      if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

     return day > 0 && day <= monthLength[month - 1];
}

function buildMsgBox(){
	$('#msgDialog').dialog({
		  autoOpen:false,
	      modal: true,	      
	      buttons:[{text: $.i18n.prop('dialog.close'), click: function() { $(this).dialog("close");} }]
	    });		
}

function messageBox(title,msg){		
	$('#msgDialog p').text(msg);	
	$('#msgDialog').dialog('option','title',title).dialog('open');	
}

function showHideAdSection(){
	
	var adSesction =  $('#adSection');
	
	if($('#showAdSection').val() == "true"){
		adSesction.show();
	}else{
		adSesction.hide();
	}
}


function loginAndRegistration(){
	
	var defaultLocal = 'en';
    var local = $('#local').val();
    if(local == 'null' || local == 'undefined'){
    	local = defaultLocal;
    }		
	
	 $.i18n.properties({
		    name:'msgJS', 
		    path:'resources/js_msg_reg/', 
		    mode:'both',
		    language: local, 
		    callback: function() {}
		 });
}

function validateEmail() {
	var value = $('#email').val();
	var pat =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return pat.test(value);
}

//function i18nPlug(xlang){	
//$.i18n.properties({
// name:'messages', 
// path:'/js_messages/', 
// mode:'map',
// language: xlang
//		 });
//}

function IEVersion() {
	var rv = -1; 
	if (navigator.appName == 'Microsoft Internet Explorer') {
		var ua = navigator.userAgent;
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null)
			rv = parseFloat(RegExp.$1);
	}
	return rv;
}

//function initI18n(){
//	$.i18n.properties({
//	    name:'messages', 
//	    path:'/js_messages/', 
//	    mode:'map',
//	    language: $('#userLanguage').val(),
//	    callback: function() {}
//	 }); 
//	}

function  monthDays(month,year){
	  return (new Date(year,month,0)).getDate();
	}