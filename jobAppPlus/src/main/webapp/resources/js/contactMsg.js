$(document).ready(function() {
	//$("#sA").hide();
	fetchMsg();	
});


function fetchMsg(){    
	$('.rms').on("click",function (event) {			
		
		var j = $(this).attr('m');
		
		$.blockUI({ message: '<h1><img src="/js/images/loading.gif" width="100px" height="15px" />'+$.i18n.prop('loadingMsg')+'</h1>' });
		
		$.ajax({
			type : "POST",
			url : "/app/Contact/readMsg",
			data : "i="+ j,
			dataType:"json",
			success : function(data) {
				$.unblockUI();
				if(data != null){					
					 showResult(data,j);
				}else{
					messageBox($.i18n.prop('err'),$.i18n.prop('errMsg'));
				}				
				
			},
			error : function() {
				$.unblockUI();
				messageBox($.i18n.prop('err'),$.i18n.prop('errorAjaxCall'));
			}
		});			
		
	});	
	
}


function w(){
	messageBox($.i18n.prop('warning'),$.i18n.prop('conx.msg'));
}

function showResult(d,j){
	
	$('#_e').text(d.email);
	$('#_d').text(d.created);
	$('#_s').text(d.subject);
	$('#_b').val(d.message);
	
	$("#sA").dialog({
	      resizable: false,
	      height:450,
	      width:700,
	      modal: true,
	      buttons:[{text: $.i18n.prop('dialog.close'), click: function() { $( this ).dialog("close");} }] 
	    });
	
	$('a[m='+j+']').find('span').remove();
    		
}