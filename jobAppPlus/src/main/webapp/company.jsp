<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt'%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<c:set var="req" value="${pageContext.request.contextPath}" />
<title id="registerTitle">Register</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="${req}/resources/css/login.css" type="text/resources/js">
<link rel="stylesheet" href="${req}/resources/css/job.css" type="text/css">

</head>
<body>
	<div id="wrapper">
		<div id="header">
		<img src="${req}/resources/js/images/d.png" width="159px" height="52px" style="float:left" />
		 	<span id="langJS" style="float: right"> <a href="app/userLang?lang=sr&p=rc">Bosanski</a>
		 	    |<a href="app/userLang?lang=en&p=rc">English</a>				
				| <a href="/app/"><img src="${req}/resources/css/images/home.png" width="20px" height="20px" style="vertical-align: bottom;"></a> 
			</span>		
		</div>

		<div id="contentLog">
			<ul class="backx">
				<li>
					<div id="login-box-right">

						<H2 id="boxTitleRegister">Register</H2>
						<p id="rmsg"></p>
						
						<form id="regForm" name="freg" action="" method="POST"
							onsubmit="donothing()">

							<ul class="formul">
								<li><label class="description" id="cn">Company Name</label> <input
									id="rcompany" name="rcompany" type="text" maxlength="60" style="width: 250px; padding: 2px" />
								</li>
								<li><label class="description1" id="cty">Country</label> <select id="rcountry" name="rcountry">
<option value="BOSNIA AND HERCEGOVINA">BOSNIA AND HERCEGOVINA</option>
<option value="AALAND ISLANDS">AALAND ISLANDS</option>
<option value="AFGHANISTAN">AFGHANISTAN</option>
<option value="ALBANIA">ALBANIA</option>
<option value="ALGERIA">ALGERIA</option>
<option value="AMERICAN SAMOA">AMERICAN SAMOA</option>
<option value="ANDORRA">ANDORRA</option>
<option value="ANGOLA">ANGOLA</option>
<option value="ANGUILLA">ANGUILLA</option>
<option value="ANTARCTICA">ANTARCTICA</option>
<option value="ANTIGUA AND BARBUDA">ANTIGUA AND BARBUDA</option>
<option value="ARGENTINA">ARGENTINA</option>
<option value="ARMENIA">ARMENIA</option>
<option value="ARUBA">ARUBA</option>
<option value="AUSTRALIA">AUSTRALIA</option>
<option value="AUSTRIA">AUSTRIA</option>
<option value="AZERBAIJAN">AZERBAIJAN</option>
<option value="BAHAMAS">BAHAMAS</option>
<option value="BAHRAIN">BAHRAIN</option>
<option value="BANGLADESH">BANGLADESH</option>
<option value="BARBADOS">BARBADOS</option>
<option value="BELARUS">BELARUS</option>
<option value="BELGIUM">BELGIUM</option>
<option value="BELIZE">BELIZE</option>
<option value="BENIN">BENIN</option>
<option value="BERMUDA">BERMUDA</option>
<option value="BHUTAN">BHUTAN</option>
<option value="BOLIVIA">BOLIVIA</option>
<option value="BOTSWANA">BOTSWANA</option>
<option value="BOUVET ISLAND">BOUVET ISLAND</option>
<option value="BRAZIL">BRAZIL</option>
<option value="BRITISH INDIAN OCEAN TERRITORY">BRITISH INDIAN OCEAN TERRITORY</option>
<option value="BRUNEI DARUSSALAM">BRUNEI DARUSSALAM</option>
<option value="BULGARIA">BULGARIA</option>
<option value="BURKINA FASO">BURKINA FASO</option>
<option value="BURUNDI">BURUNDI</option>
<option value="CAMBODIA">CAMBODIA</option>
<option value="CAMEROON">CAMEROON</option>
<option value="CANADA">CANADA</option>
<option value="CAPE VERDE">CAPE VERDE</option>
<option value="CAYMAN ISLANDS">CAYMAN ISLANDS</option>
<option value="CENTRAL AFRICAN REPUBLIC">CENTRAL AFRICAN REPUBLIC</option>
<option value="CHAD">CHAD</option>
<option value="CHILE">CHILE</option>
<option value="CHINA">CHINA</option>
<option value="CHRISTMAS ISLAND">CHRISTMAS ISLAND</option>
<option value="COCOS (KEELING) ISLANDS">COCOS (KEELING) ISLANDS</option>
<option value="COLOMBIA">COLOMBIA</option>
<option value="COMOROS">COMOROS</option>
<option value="CONGO DEMOCRATIC REPUBLIC">CONGO DEMOCRATIC REPUBLIC</option>
<option value="CONGO REPUBLIC">CONGO REPUBLIC</option>
<option value="COOK ISLANDS">COOK ISLANDS</option>
<option value="COSTA RICA">COSTA RICA</option>
<option value="COTE D'IVOIRE">COTE D'IVOIRE</option>
<option value="CROATIA">CROATIA</option>
<option value="CUBA">CUBA</option>
<option value="CYPRUS">CYPRUS</option>
<option value="CZECH REPUBLIC">CZECH REPUBLIC</option>
<option value="DENMARK">DENMARK</option>
<option value="DJIBOUTI">DJIBOUTI</option>
<option value="DOMINICA">DOMINICA</option>
<option value="DOMINICAN REPUBLIC">DOMINICAN REPUBLIC</option>
<option value="ECUADOR">ECUADOR</option>
<option value="EGYPT">EGYPT</option>
<option value="EL SALVADOR">EL SALVADOR</option>
<option value="EQUATORIAL GUINEA">EQUATORIAL GUINEA</option>
<option value="ERITREA">ERITREA</option>
<option value="ESTONIA">ESTONIA</option>
<option value="ETHIOPIA">ETHIOPIA</option>
<option value="FALKLAND ISLANDS (MALVINAS)">FALKLAND ISLANDS (MALVINAS)</option>
<option value="FAROE ISLANDS">FAROE ISLANDS</option>
<option value="FIJI">FIJI</option>
<option value="FINLAND">FINLAND</option>
<option value="FRANCE">FRANCE</option>
<option value="FRENCH GUIANA">FRENCH GUIANA</option>
<option value="FRENCH POLYNESIA">FRENCH POLYNESIA</option>
<option value="FRENCH SOUTHERN TERRITORIES">FRENCH SOUTHERN TERRITORIES</option>
<option value="GABON">GABON</option>
<option value="GAMBIA">GAMBIA</option>
<option value="GEORGIA">GEORGIA</option>
<option value="GERMANY">GERMANY</option>
<option value="GHANA">GHANA</option>
<option value="GIBRALTAR">GIBRALTAR</option>
<option value="GREECE">GREECE</option>
<option value="GREENLAND">GREENLAND</option>
<option value="GRENADA">GRENADA</option>
<option value="GUADELOUPE">GUADELOUPE</option>
<option value="GUAM">GUAM</option>
<option value="GUATEMALA">GUATEMALA</option>
<option value="GUINEA">GUINEA</option>
<option value="GUINEA-BISSAU">GUINEA-BISSAU</option>
<option value="GUYANA">GUYANA</option>
<option value="HAITI">HAITI</option>
<option value="HEARD AND MC DONALD ISLANDS">HEARD AND MC DONALD ISLANDS</option>
<option value="HONDURAS">HONDURAS</option>
<option value="HONG KONG">HONG KONG</option>
<option value="HUNGARY">HUNGARY</option>
<option value="ICELAND">ICELAND</option>
<option value="INDIA">INDIA</option>
<option value="INDONESIA">INDONESIA</option>
<option value="IRAN (ISLAMIC REPUBLIC OF)">IRAN (ISLAMIC REPUBLIC OF)</option>
<option value="IRAQ">IRAQ</option>
<option value="IRELAND">IRELAND</option>
<option value="ISRAEL">ISRAEL</option>
<option value="ITALY">ITALY</option>
<option value="JAMAICA">JAMAICA</option>
<option value="JAPAN">JAPAN</option>
<option value="JORDAN">JORDAN</option>
<option value="KAZAKHSTAN">KAZAKHSTAN</option>
<option value="KENYA">KENYA</option>
<option value="KIRIBATI">KIRIBATI</option>
<option value="KOREA DEMOCRATIC PEOPLE'S REPUBLIC">KOREA DEMOCRATIC PEOPLE'S REPUBLIC</option>
<option value="SOUTH KOREA">SOUTH KOREA</option>
<option value="KUWAIT">KUWAIT</option>
<option value="KYRGYZSTAN">KYRGYZSTAN</option>
<option value="LAO PEOPLE'S DEMOCRATIC REPUBLIC">LAO PEOPLE'S DEMOCRATIC REPUBLIC</option>
<option value="LATVIA">LATVIA</option>
<option value="LEBANON">LEBANON</option>
<option value="LESOTHO">LESOTHO</option>
<option value="LIBERIA">LIBERIA</option>
<option value="LIBYAN ARAB JAMAHIRIYA">LIBYAN ARAB JAMAHIRIYA</option>
<option value="LIECHTENSTEIN">LIECHTENSTEIN</option>
<option value="LITHUANIA">LITHUANIA</option>
<option value="LUXEMBOURG">LUXEMBOURG</option>
<option value="MACAU">MACAU</option>
<option value="MACEDONIA">MACEDONIA</option>
<option value="MADAGASCAR">MADAGASCAR</option>
<option value="MALAWI">MALAWI</option>
<option value="MALAYSIA">MALAYSIA</option>
<option value="MALDIVES">MALDIVES</option>
<option value="MALI">MALI</option>
<option value="MALTA">MALTA</option>
<option value="MARSHALL ISLANDS">MARSHALL ISLANDS</option>
<option value="MARTINIQUE">MARTINIQUE</option>
<option value="MAURITANIA">MAURITANIA</option>
<option value="MAURITIUS">MAURITIUS</option>
<option value="MAYOTTE">MAYOTTE</option>
<option value="MEXICO">MEXICO</option>
<option value="MICRONESIA FEDERATED STATES">MICRONESIA FEDERATED STATES</option>
<option value="MOLDOVA REPUBLIC">MOLDOVA REPUBLIC</option>
<option value="MONACO">MONACO</option>
<option value="MONGOLIA">MONGOLIA</option>
<option value="MONTSERRAT">MONTSERRAT</option>
<option value="MOROCCO">MOROCCO</option>
<option value="MOZAMBIQUE">MOZAMBIQUE</option>
<option value="MYANMAR">MYANMAR</option>
<option value="NAMIBIA">NAMIBIA</option>
<option value="NAURU">NAURU</option>
<option value="NEPAL">NEPAL</option>
<option value="NETHERLANDS">NETHERLANDS</option>
<option value="NETHERLANDS ANTILLES">NETHERLANDS ANTILLES</option>
<option value="NEW CALEDONIA">NEW CALEDONIA</option>
<option value="NEW ZEALAND">NEW ZEALAND</option>
<option value="NICARAGUA">NICARAGUA</option>
<option value="NIGER">NIGER</option>
<option value="NIGERIA">NIGERIA</option>
<option value="NIUE">NIUE</option>
<option value="NORFOLK ISLAND">NORFOLK ISLAND</option>
<option value="NORTHERN MARIANA ISLANDS">NORTHERN MARIANA ISLANDS</option>
<option value="NORWAY">NORWAY</option>
<option value="OMAN">OMAN</option>
<option value="PAKISTAN">PAKISTAN</option>
<option value="PALAU">PALAU</option>
<option value="PALESTINIAN TERRITORY">PALESTINIAN TERRITORY</option>
<option value="PANAMA">PANAMA</option>
<option value="PAPUA NEW GUINEA">PAPUA NEW GUINEA</option>
<option value="PARAGUAY">PARAGUAY</option>
<option value="PERU">PERU</option>
<option value="PHILIPPINES">PHILIPPINES</option>
<option value="PITCAIRN">PITCAIRN</option>
<option value="POLAND">POLAND</option>
<option value="PORTUGAL">PORTUGAL</option>
<option value="PUERTO RICO">PUERTO RICO</option>
<option value="QATAR">QATAR</option>
<option value="REUNION">REUNION</option>
<option value="ROMANIA">ROMANIA</option>
<option value="RUSSIAN FEDERATION">RUSSIAN FEDERATION</option>
<option value="RWANDA">RWANDA</option>
<option value="SAINT HELENA">SAINT HELENA</option>
<option value="SAINT KITTS AND NEVIS">SAINT KITTS AND NEVIS</option>
<option value="SAINT LUCIA">SAINT LUCIA</option>
<option value="SAINT PIERRE AND MIQUELON">SAINT PIERRE AND MIQUELON</option>
<option value="SAINT VINCENT AND THE GRENADINES">SAINT VINCENT AND THE GRENADINES</option>
<option value="SAMOA">SAMOA</option>
<option value="SAN MARINO">SAN MARINO</option>
<option value="SAO TOME AND PRINCIPE">SAO TOME AND PRINCIPE</option>
<option value="SAUDI ARABIA">SAUDI ARABIA</option>
<option value="SENEGAL">SENEGAL</option>
<option value="SERBIA AND MONTENEGRO">SERBIA AND MONTENEGRO</option>
<option value="SEYCHELLES">SEYCHELLES</option>
<option value="SIERRA LEONE">SIERRA LEONE</option>
<option value="SINGAPORE">SINGAPORE</option>
<option value="SLOVAKIA">SLOVAKIA</option>
<option value="SLOVENIA">SLOVENIA</option>
<option value="SOLOMON ISLANDS">SOLOMON ISLANDS</option>
<option value="SOMALIA">SOMALIA</option>
<option value="SOUTH AFRICA">SOUTH AFRICA</option>
<option value="SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS">SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS</option>
<option value="SPAIN">SPAIN</option>
<option value="SRI LANKA">SRI LANKA</option>
<option value="SUDAN">SUDAN</option>
<option value="SURINAME">SURINAME</option>
<option value="SVALBARD AND JAN MAYEN ISLANDS">SVALBARD AND JAN MAYEN ISLANDS</option>
<option value="SWAZILAND">SWAZILAND</option>
<option value="SWEDEN">SWEDEN</option>
<option value="SWITZERLAND">SWITZERLAND</option>
<option value="SYRIAN ARAB REPUBLIC">SYRIAN ARAB REPUBLIC</option>
<option value="TAIWAN">TAIWAN</option>
<option value="TAJIKISTAN">TAJIKISTAN</option>
<option value="TANZANIA UNITED REPUBLIC OF">TANZANIA UNITED REPUBLIC OF</option>
<option value="THAILAND">THAILAND</option>
<option value="TIMOR-LESTE">TIMOR-LESTE</option>
<option value="TOGO">TOGO</option>
<option value="TOKELAU">TOKELAU</option>
<option value="TONGA">TONGA</option>
<option value="TRINIDAD AND TOBAGO">TRINIDAD AND TOBAGO</option>
<option value="TUNISIA">TUNISIA</option>
<option value="TURKEY">TURKEY</option>
<option value="TURKMENISTAN">TURKMENISTAN</option>
<option value="TURKS AND CAICOS ISLANDS">TURKS AND CAICOS ISLANDS</option>
<option value="TUVALU">TUVALU</option>
<option value="UGANDA">UGANDA</option>
<option value="UKRAINE">UKRAINE</option>
<option value="UNITED ARAB EMIRATES">UNITED ARAB EMIRATES</option>
<option value="UNITED KINGDOM">UNITED KINGDOM</option>
<option value="UNITED STATES">UNITED STATES</option>
<option value="UNITED STATES MINOR OUTLYING ISLANDS">UNITED STATES MINOR OUTLYING ISLANDS</option>
<option value="URUGUAY">URUGUAY</option>
<option value="UZBEKISTAN">UZBEKISTAN</option>
<option value="VANUATU">VANUATU</option>
<option value="VATICAN CITY STATE">VATICAN CITY STATE</option>
<option value="VENEZUELA">VENEZUELA</option>
<option value="VIET NAM">VIET NAM</option>
<option value="VIRGIN ISLANDS (BRITISH)">VIRGIN ISLANDS (BRITISH)</option>
<option value="VIRGIN ISLANDS (U.S.)">VIRGIN ISLANDS (U.S.)</option>
<option value="WALLIS AND FUTUNA ISLANDS">WALLIS AND FUTUNA ISLANDS</option>
<option value="WESTERN SAHARA">WESTERN SAHARA</option>
<option value="YEMEN">YEMEN</option>
<option value="ZAMBIA">ZAMBIA</option>
<option value="ZIMBABWE">ZIMBABWE</option>
<option value="WORLD WIDE">WORLD WIDE</option>
</select></li>	
<li><label class="description" id="ct">Company Type</label>
 <select id="rcompanyType" name="rcompanyType">
	<option value="" ></option>
	<option am="1" value="Private">Private</option>
	<option am="2" value="Public">Public</option>
	<option am="3" value="Non-profit Organization">Non-profit Organization</option>
	<option am="4" value="Recruitment Agency">Recruitment Agency</option>
 </select></li>

									
									<li><label class="description" id="pn">Phone Number</label>
										<input id="rphoneNo" name="rphoneNo" type="text" maxlength="16" />									
									</li>																			
									

								<li><label class="description2" id="ea">Email Address</label> <input
									id="remail" name="remail" type="text" maxlength="60" style="width: 300px; padding: 2px" />
								</li>
								<li><label class="description" id="n">Name</label> <span>
										<input id="rname" name="rname" maxlength="50" size="14"
										value="" /><label id="fT">First</label></span> <span> <input
										id="rsurname" name="rsurname" maxlength="50" size="14"
										value="" /><label id="lT">Last</label></span></li>
								<li><label class="description" id="pw">Password (minimum 9 characters)</label>
								    <input id="rpassword" name="rpassword" type="password" maxlength="50" value="" /></li>
								<li><label class="description" id="cp">Confirm Password</label> <input
									id="rcpassword" name="rcpassword" type="password"
									maxlength="50" value="" /></li>
								<li></li>
								
								<li></li>							
								<li><img src="<c:out value="${applicationServerAddress}"/>/${req}/captcha"
									class="captchax" id="capx"> <input type="button"
									value="Refresh" id="refresh" class="refreshButton"></li>
								<li><label class="description" id="catpchaTitle">Enter the symbols</label> <input
									type="text" name="code" id="code" /></li>
								<li><input type="submit"  alt="Submit button" value="Register" class="loginRegButton" style="margin-left: 220px;" id="regBtn"></li>
							</ul>
						</form>

					</div></li>
			</ul>

		</div>
<input type="hidden" id="local" value="<%= session.getAttribute( "local" ) %>"/>

</div>
<div id="scriptDiv">
<script src="${req}/resources/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" 	src="${req}/resources/js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="${req}/resources/js/commonStaffReg.js"></script>
<script type="text/javascript" src="${req}/resources/js/jquery.i18n.properties-min-1.0.9.js"></script>
<script type="text/javascript" src="${req}/resources/js/sha256.js"></script>

<script type="text/javascript">
	$(document).ready(function() {	
		
		loginAndRegistration();
		$('#rmonth').change(function(){calc();});
		$('#ryear').change(function(){calc();});
		
		 if(!(local == 'null' || local == 'undefined')){			
			$('#ea').text($.i18n.prop('regi.emailAddress'));
			$('#n').text( $.i18n.prop('regi.name'));
			$('#fT').text($.i18n.prop('regi.first'));
			$('#lT').text($.i18n.prop('regi.last'));
			$('#pw').text($.i18n.prop('regi.password'));
			$('#cp').text($.i18n.prop('regi.confirmPassword'));						
			$('[name=rcountry] option:eq(0)').text($.i18n.prop('regi.BH'));
			$('#cn').text($.i18n.prop('regi.company'));
			$('#cty').text($.i18n.prop('regi.country'));
			$('#ct').text($.i18n.prop('regi.companyType'));	
			$('#pn').text($.i18n.prop('regi.phone'));	
			$('#catpchaTitle').text($.i18n.prop('regi.captcha'));
			$('#refresh').val($.i18n.prop('regi.refresh'));
			$('#regBtn').val($.i18n.prop('regi.registerButton'));
			$('#registerTitle').text($.i18n.prop('registerTitle'));
			$('#boxTitleRegister').text($.i18n.prop('regi.registerButton'));
			    
			 }

						$('#refresh').click(function(event) {
											<!--$("#capx").attr("src","<c:out value="${applicationServerAddress}"/>/captcha?"+Math.floor(Math.random()*11000));-->
											$("#capx").attr("src","${req}/captcha?"+Math.floor(Math.random()*11000));
										});

						function validateEmail(elementValue) {
							var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
							return emailPattern.test(elementValue);
						}

						function writeMessage(message) {
							$("#rmsg").text(message).css("color", "#CC3333").css("font-size", "18px").css("font-family","\'Tahoma\'").css("background-color", "#FFCC18");
							xcall = "false";
							return false;
						}

$('#regBtn').click(function(event) {
				event.preventDefault();
				var xcall = "true";
				
				if ($('#rcompany').val() == "") {
					return writeMessage($.i18n.prop('err.company'));
				}
				
				if ($('#rcountry').val() == "") {
					return writeMessage($.i18n.prop('err.country'));
				}
				
				if ($('#rcompanyType').val() == "") {
					return writeMessage($.i18n.prop('err.companyType'));
				}
				
				if ($('#rphoneNo').val() == "") {
					return writeMessage($.i18n.prop('err.phone'));
				}

			   if (validateEmail($('#remail').val()) == false) {
					return writeMessage($.i18n.prop('rErr.email'));
							}

											if ($('#remail').val().substring(
													0,
													$('#remail').val().indexOf(
															"@")).length < 3
													|| /^[a-zA-Z]*$/.test($(
															'#remail').val()
															.substring(0, 3)) == false) {
												return writeMessage($.i18n.prop('rErr.emailformat'));
											}

											if ($('#rname').val().length < 1) {
												return writeMessage($.i18n.prop('rErr.name'));
											}

											if ($('#rsurname').val().length < 1) {
												return writeMessage($.i18n.prop('rErr.surname'));
											}

											if ($("#rpassword").val().length < 9) {
												return writeMessage($.i18n.prop('rErr.password'));
											}

											if ($("#rpassword").val() != $(
													"#rcpassword").val()) {
												return writeMessage($.i18n.prop('rErr.confirmPassword'));
											}

											if (xcall == "true") {

												$.ajax({
															type : "POST",
															url : "/app/register/cr",
															data : "email="+ $('#remail').val()
																	+ "&name="+$('#rname').val()
																	+ "&surname="+ $("#rsurname").val()
																	+ "&password="+ sha256_digest($("#rpassword").val())
																	+ "&company="+ $("#rcompany").val()																	
																	+ "&companyType="+ $("#rcompanyType").val()
																	+ "&phone="+ $("#rphoneNo").val()
																	+ "&country="+ $("#rcountry option:selected").val()
																	+ "&code="+ $("#code").val(),
															success : function(
																	data) {
																if (data == $.i18n.prop('regi.ok.signal')) {																	
																	$("#rmsg").text($.i18n.prop('regi.ok.successfulRegistration')).css('color','rgb(97,162,97)').css('font-size','17').css('padding-left','5px');

																	$('#remail').val("");
																	$('#rname').val("");
																	$('#rsurname').val("");
																	$('#rpassword').val("");
																	$('#rcpassword').val("");
																	$('#ryear').val("");
																	$("#rmonth").find('option:first').attr('selected','selected');
																	$("#rday").find('option:first').attr('selected','selected');
																	$("#rgender").find('option:first').attr('selected','selected');
																	$("#rcountry").find('option:first').attr('selected','selected');
																}else if(data == $.i18n.prop('rErr.alreadyRegistered')){																
																	writeMessage($.i18n.prop('regi.msg.alreadyRegistered'));																
															    }else if(data == "Please enter symbols correctly"){ 
																	writeMessage($.i18n.prop('rErr.captcha'));
																}else if(data = "Company_Exist"){
																	writeMessage($.i18n.prop('regi.companyRegistered'));																	
																} else
																	console.log($.i18n.prop('err')+ data);
															}
														});

											}

										});

					});

function hashP() {
		var hashx = sha256_digest($("#log_pass").val());
		$("#log_pass").val(hashx);}

function calc(){		
		if(checkYear()) return;
		
		var no = monthDays($('#rmonth option:selected').attr('am'),$('#ryear').val());
		$('#rday option').remove();
		$('#rday').append('<option value=""></option>');
		for(var  i =1; i<=no; i++){
			$('#rday').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	
function checkYear(){
		return $('#ryear').val().length != 4 || /^[0-9]*$/.test($('#ryear').val()) == false || parseInt($('#ryear').val()) < 1910 || parseInt($('#ryear').val()) > 2012;		
	}
	
function donothing() {return false;}
</script>

</div>

</body>
</html>