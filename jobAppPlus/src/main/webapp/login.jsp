<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<c:set var="req" value="${pageContext.request.contextPath}" />

<title id="loginTitle">Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="${req}/resources/css/newlogin.css" type="text/css">
</head>

<body>
<input type="hidden" value="${req}" id="contextPath"/>
	<div id="wrapper">

		<div id="header">
		<img src="${req}/resources/js/images/logo.png" width="159px" height="52px" style="float:left" />
			<span id="langJS" style="float: right"><a href="${req}/web/userLang?lang=cr">Hrvatski</a>				
				| <a href="${req}/web/userLang?lang=en">English</a>
				| <a href="${req}/web/home/"><img src="${req}/resources/css/images/home.png" width="20px" height="20px" style="vertical-align: bottom;"></a> 
			</span>

		</div>

			<div id="login" class="login">
					<div class="login-screen">

				<div class="app-title">
				    <h1>Login</h1>
		        </div>
					<div class="login-form">	
						<form id="ax" name="f" data-parsley-validate data-parsley-required
							action="<c:url value='j_spring_security_check'/>" method="POST"
							onsubmit="hashP()">
							<div id="un">Username</div>
							<div>
								<input name="j_username" title="Username"
									id="log_username" size="30" maxlength="2048"
									value="alaa@gmail.com" data-parsley-type="email" data-parsley-trigger="change" />
							</div>
							<div id="pw">Password</div>
							<div>
								<input name="j_password" type="password"
									title="Password" id="log_pass" size="30" maxlength="2048"
									value="test1" data-parsley-minlength="6" data-parsley-trigger="change" />
							</div>						
							<input type="submit" id="submitBtn" alt="Submit button" class="btn btn-primary btn-large btn-block" value="Login"/>
							
							<br /> 
							<span>						
								<a href="${req}/web/fpp/" id="fp">Forgot password?</a>
							</span> <br /> <br />
						</form>
                      </div>
					</div>
					<h2 id='kw' style='text-align:center; color:rgb(89, 129, 134)'></h2>
		</div>

<input type="hidden" id="local" value="<%= session.getAttribute( "local" ) %>"/>
	</div>

<div id="scriptDiv">

<script src="${req}/resources/js/jquery-1.12.0.js"></script>
<script type="text/javascript" src="${req}/resources/js/sha256.js"></script>
<script type="text/javascript" src="${req}/resources/js/commonStaff.js"></script>
<script src="${req}/resources/js/parsley-2.3.3.js"></script>
<script type="text/javascript" src="${req}/resources/js/jquery.i18n.properties.js"></script>

<script type="text/javascript">
$(document).ready(function() {	
	
	internatinalization();
	
	if(!(local == 'null' || local == 'undefined')){
	    $('#un').text($.i18n.prop('login.username'));
	    $('#pw').text($.i18n.prop('login.password'));	    
	    $('#fp').text($.i18n.prop('login.forgotPassword'));
	    $('#submitBtn').val($.i18n.prop('login.loginButton'));
	    $('#loginTitle').text($.i18n.prop('loginTitle'));
	    $('#boxTitleLogin').text($.i18n.prop('loginTitle'));
	    $('#f').text($.i18n.prop('login.err')+ $('#f').text());	    
	 }
		 
function writeMessage(message) {
		$("#rmsg").text(message).css("color", "red");
		xcall = "false";
		return false;
	}
});

function hashP() {
		var hashx = sha256_digest($("#log_pass").val());
		$("#log_pass").val(hashx);
	}

function donothing() {return false;}

$('#ax').parsley("validate");
Parsley.setLocale($('#local').val());


</script>

</div>

</body>
</html>