<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jstl/core_rt'  prefix='c'%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<c:set var="req" value="${pageContext.request.contextPath}" />
<title id="registerTitle">Register</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="${req}/resources/css/login.css" type="text/css">
<link rel="stylesheet" href="${req}/resources/css/car.css" type="text/css">

</head>
<body>
<input type="hidden" value="${req}" id="contextPath"/>
	<div id="wrapper">
		<div id="header">
		<img src="${req}/resources/js/images/carlogono.png" width="159px" height="52px" style="float:left" />
		 	<span id="langJS" style="float: right"> <a href="${req}/web/userLang?lang=sr&p=r">Bosanski</a>
		 	    |<a href="${req}/web/userLang?lang=en&p=r">English</a>				
				| <a href="${req}/web/first/"><img src="${req}/resources/css/images/home.png" width="20px" height="20px" style="vertical-align: bottom;"></a> 
			</span>
		
		</div>

		<div id="contentLog">

			<ul class="backx">
				<li>

					<div id="login-box-right">

						<H2 id="boxTitleRegister">Register</H2>
						<p id="rmsg"></p>
						
						<form id="regForm" name="freg" action="" method="POST"
							onsubmit="donothing()">

							<ul class="formul">
								<li><label class="description" id="ea">Email Address</label> <input
									id="remail" name="remail" type="text" maxlength="60" value="ams@gmail.com" />
								</li>
								<li><label class="description" id="n">Name</label> <span>
										<input id="rname" name="rname" maxlength="50" size="14"
										value="ams" /><label id="fT">First</label></span> <span> <input
										id="rsurname" name="rsurname" maxlength="50" size="14"
										value="amx" /><label id="lT">Last</label></span></li>
								<li><label class="description" id="pw">Password (minimum 9 characters)</label>
								    <input id="rpassword" name="rpassword" type="password" maxlength="50" value="123456789" /></li>
								<li><label class="description" id="cp">Confirm Password</label> <input
									id="rcpassword" name="rcpassword" type="password"
									maxlength="50" value="123456789" /></li>
								<li>
									<ul class="birthday">
									<li><label class="description1" id="yr">Year</label> <input
											class="sizex" id="ryear" name="ryear" type="text"
											maxlength="4" value="1950" /></li>
										<li><label class="description1" id="mo">month</label> <select
											id="rmonth" name="rmonth">
												<option value="" ></option>
												<option am="1" value="Jan">Jan</option>
												<option am="2" value="Feb">Feb</option>
												<option am="3" value="Mar">Mar</option>
												<option am="4" value="Apr">Apr</option>
												<option am="5" value="May">May</option>
												<option am="6" value="Jun">Jun</option>
												<option am="7" value="Jul">Jul</option>
												<option am="8" value="Aug">Aug</option>
												<option am="9" value="Sep">Sep</option>
												<option am="10" value="Oct">Oct</option>
												<option am="11" value="Nov">Nov</option>
												<option am="12" value="Dec">Dec</option>

										</select></li>
										<li><label class="description1" id="da">day</label> <select
											id="rday" name="rday">
												<option value="" ></option>
												<option value="1" selected="selected">01</option>
												<option value="2">02</option>
												<option value="3">03</option>
												<option value="4">04</option>
												<option value="5">05</option>
												<option value="6">06</option>
												<option value="7">07</option>
												<option value="8">08</option>
												<option value="9">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
												<option value="13">13</option>
												<option value="14">14</option>
												<option value="15">15</option>
												<option value="16">16</option>
												<option value="17">17</option>
												<option value="18">18</option>
												<option value="19">19</option>
												<option value="20">20</option>
												<option value="21">21</option>
												<option value="22">22</option>
												<option value="23">23</option>
												<option value="24">24</option>
												<option value="25">25</option>
												<option value="26">26</option>
												<option value="27">27</option>
												<option value="28">28</option>
												<option value="29">29</option>
												<option value="30">30</option>
												<option value="31">31</option>
										</select></li>
										
									</ul></li>
								<li><label class="description1" id="gn">Gender</label> <select
									id="rgender" name="rgender">
										<option value="" ></option>
										<option value="male">Male</option>
										<option value="female">Female</option>
								</select></li>							
								<li><img src="<c:out value="${applicationServerAddress}"/>${req}/captcha"
									class="captchax" id="capx"> <input type="button"
									value="Refresh" id="refresh" class="refreshButton"></li>
								<li><label class="description" id="catpchaTitle">Enter the symbols</label> <input
									type="text" name="code" id="code" /></li>
								<li><input type="submit"  alt="Submit button" value="Register" class="loginRegButton" style="margin-left: 220px;" id="regBtn"></li>
							</ul>
						</form>

					</div></li>
			</ul>

		</div>
<input type="hidden" id="local" value="<%= session.getAttribute( "local" ) %>"/>

	</div>
<div id="scriptDiv">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" 	src="${req}/resources/js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="${req}/resources/js/commonStaff.js"></script>
<script type="text/javascript" src="${req}/resources/js/jquery.i18n.properties-min-1.0.9.js"></script>
<script type="text/javascript" src="${req}/resources/js/sha256.js"></script>


<script type="text/javascript">
	$(document).ready(function() {	
		
		loginAndRegistration();
		$('#rmonth').change(function(){calc();});
		$('#ryear').change(function(){calc();});
		
		 if(!(local == 'null' || local == 'undefined')){			
			$('#ea').text($.i18n.prop('regi.emailAddress'));
			$('#n').text( $.i18n.prop('regi.name'));
			$('#fT').text($.i18n.prop('regi.first'));
			$('#lT').text($.i18n.prop('regi.last'));
			$('#pw').text($.i18n.prop('regi.password'));
			$('#cp').text($.i18n.prop('regi.confirmPassword'));
			$('#mo').text($.i18n.prop('regi.month'));
			$('[name=rmonth] option:eq(1)').text($.i18n.prop('regi.jan'));
			$('[name=rmonth] option:eq(2)').text($.i18n.prop('regi.feb'));
			$('[name=rmonth] option:eq(3)').text($.i18n.prop('regi.mar'));
			$('[name=rmonth] option:eq(4)').text($.i18n.prop('regi.apr'));
			$('[name=rmonth] option:eq(5)').text($.i18n.prop('regi.may'));
			$('[name=rmonth] option:eq(6)').text($.i18n.prop('regi.jun'));
			$('[name=rmonth] option:eq(7)').text($.i18n.prop('regi.jul'));
			$('[name=rmonth] option:eq(8)').text($.i18n.prop('regi.aug'));
			$('[name=rmonth] option:eq(9)').text($.i18n.prop('regi.sep'));
			$('[name=rmonth] option:eq(10)').text($.i18n.prop('regi.oct'));
			$('[name=rmonth] option:eq(11)').text($.i18n.prop('regi.nov'));
			$('[name=rmonth] option:eq(12)').text($.i18n.prop('regi.dec'));
			$('#da').text($.i18n.prop('regi.day'));
			$('#yr').text($.i18n.prop('regi.year'));
			$('#gn').text($.i18n.prop('regi.gendar'));
			$('[name=rgender] option:eq(1)').text($.i18n.prop('regi.male'));
			$('[name=rgender] option:eq(2)').text($.i18n.prop('regi.female'));
			$('#catpchaTitle').text($.i18n.prop('regi.captcha'));
			$('#refresh').val($.i18n.prop('regi.refresh'));
			$('#regBtn').val($.i18n.prop('regi.registerButton'));
			$('#registerTitle').text($.i18n.prop('registerTitle'));
			$('#boxTitleRegister').text($.i18n.prop('regi.registerButton'));
			    
			 }

						$('#refresh').click(function(event) {
											$("#capx").attr("src","<c:out value="${applicationServerAddress}"/><c:out value="/${req}"/>/captcha?"+Math.floor(Math.random()*11000));
										});

						function validateEmail(elementValue) {
							var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
							return emailPattern.test(elementValue);
						}

						function writeMessage(message) {
							$("#rmsg").text(message).css("color", "#CC3333").css("font-size", "18px").css("font-family","\'Tahoma\'").css("background-color", "#FFCC18");
							xcall = "false";
							return false;
						}

						$('#regBtn').click(function(event) {

											event.preventDefault();

											var xcall = "true";

											if (validateEmail($('#remail')
													.val()) == false) {
												return writeMessage($.i18n.prop('rErr.email'));
											}

											if ($('#remail').val().substring(
													0,
													$('#remail').val().indexOf(
															"@")).length < 3
													|| /^[a-zA-Z]*$/.test($(
															'#remail').val()
															.substring(0, 3)) == false) {
												return writeMessage($.i18n.prop('rErr.emailformat'));
											}

											if ($('#rname').val().length < 1) {
												return writeMessage($.i18n.prop('rErr.name'));
											}

											if ($('#rsurname').val().length < 1) {
												return writeMessage($.i18n.prop('rErr.surname'));
											}

											if ($("#rpassword").val().length < 9) {
												return writeMessage($.i18n.prop('rErr.password'));
											}

											if ($("#rpassword").val() != $(
													"#rcpassword").val()) {
												return writeMessage($.i18n.prop('rErr.confirmPassword'));
											}

											if ($("#rmonth").val() == "") {
												return writeMessage($.i18n.prop('rErr.month'));
											}

											if ($("#rday").val() == "") {
												return writeMessage($.i18n.prop('rErr.day'));
											}

											if (checkYear()) {
												return writeMessage($.i18n.prop('rErr.year'));
											}

											if ($("#rgender").val() == "") {
												return writeMessage($.i18n.prop('rErr.gender'));
											}

											/*if ($("#rcountry").val() == "") {
												return writeMessage("- Select country");
											}*/

											if (xcall == "true") {

												$.ajax({
															type : "POST",
															url : "/${req}/web/register/p",
															data : "email="+ $('#remail').val()
																	+ "&name="
																	+ $('#rname').val()
																	+ "&surname="
																	+ $("#rsurname").val()
																	+ "&password="
																	+ sha256_digest($("#rpassword").val())
																	+ "&month="
																	+ $("#rmonth option:selected").text()
																	+ "&day="
																	+ $("#rday").val()
																	+ "&year="
																	+ $("#ryear").val()
																	+ "&gender="
																	+ $("#rgender option:selected").val()
																	+ "&country=JAPAN"
																	/*+ $("#rcountry option:selected").text()*/
																	+ "&code="
																	+ $("#code").val(),
															success : function(
																	data) {
																if (data == $.i18n.prop('regi.ok.signal')) {
																	
																	$("#rmsg").text($.i18n.prop('regi.ok.successfulRegistration'))
																			.css('color','rgb(97,162,97)').css('font-size','17').css('padding-left','5px');

																	$('#remail').val("");
																	$('#rname').val("");
																	$('#rsurname').val("");
																	$('#rpassword')	.val("");
																	$('#rcpassword').val("");
																	$('#ryear').val("");
																	$("#rmonth").find('option:first')
																			.attr('selected','selected');
																	$("#rday").find('option:first')
																			.attr('selected','selected');
																	$("#rgender").find('option:first')
																			.attr('selected','selected');
																	$("#rcountry").find('option:first')
																			.attr('selected','selected');
																}else if(data == $.i18n.prop('rErr.alreadyRegistered')){																
																	writeMessage($.i18n.prop('regi.msg.alreadyRegistered'));																
															    }else if(data == "Please enter symbols correctly"){ 
																	writeMessage($.i18n.prop('rErr.captcha'));
																} else
																	console.log($.i18n.prop('err')+ data);
															}
														});

											}

										});

					});

	function hashP() {

		var hashx = sha256_digest($("#log_pass").val());
		$("#log_pass").val(hashx);
	}
	
	function calc(){		
		if(checkYear()) return;
		
		var no = monthDays($('#rmonth option:selected').attr('am'),$('#ryear').val());
		$('#rday option').remove();
		$('#rday').append('<option value=""></option>');
		for(var  i =1; i<=no; i++){
			$('#rday').append('<option value="'+i+'">'+i+'</option>');
		}
	}
	
	function checkYear(){
		return $('#ryear').val().length != 4 || /^[0-9]*$/.test($('#ryear').val()) == false || parseInt($('#ryear').val()) < 1910 || parseInt($('#ryear').val()) > 2012;		
	}

	function donothing() {
		return false;
	}
</script>

</div>

</body>
</html>