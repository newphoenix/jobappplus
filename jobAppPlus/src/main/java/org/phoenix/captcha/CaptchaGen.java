package org.phoenix.captcha;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;

import java.util.Random;
import javax.imageio.ImageIO;
import javax.servlet.*;
import javax.servlet.http.*;


public class CaptchaGen extends HttpServlet {

	final private static long serialVersionUID = -9790909015826L;
	
	final private static int WIDTH = 125;
	final private static int HEIGHT = 50;
	    
	final private static char[] data ={'1','2','3','4','5','6','7','8','9','a','A','b','B','c','F','J','L','n','P','t','z','Y','v','m','x','R','w'};
	private static Random r = new Random();
	final private static String CONTENT_TYPE = "image/png";
	final private static String IMAGE_TYPE = "png";	
	final private static String FONT_TYPE = "Georgia";
	final private static String SESSION_CAPTCHA_VARIABLE = "captcha";
	final private static Font FONT = new Font(FONT_TYPE, Font.BOLD, 18);
	final private static Color COLOR = new Color(255, 153, 0);


protected void processRequest(HttpServletRequest request, 
                                HttpServletResponse response) 
                 throws ServletException, IOException {   
	
	int numberOfChars = r.nextInt(3)+3;
	char[] cap = new char[numberOfChars];
	
	for(int i =0; i< numberOfChars;i++)
		cap[i]=data[r.nextInt(data.length)];
        
    //----------------

    BufferedImage bufferedImage = new BufferedImage(WIDTH, HEIGHT, 
                  BufferedImage.TYPE_INT_RGB);

    Graphics2D g2d = bufferedImage.createGraphics();

    g2d.setFont(FONT);

    RenderingHints rh = new RenderingHints(
           RenderingHints.KEY_ANTIALIASING,
           RenderingHints.VALUE_ANTIALIAS_ON);

    rh.put(RenderingHints.KEY_RENDERING, 
           RenderingHints.VALUE_RENDER_QUALITY);

    g2d.setRenderingHints(rh);

    GradientPaint gp = new GradientPaint(0, 0, 
    Color.green, 0, 25, Color.black, true);

    g2d.setPaint(gp);
    g2d.fillRect(0, 0, WIDTH, HEIGHT);

    g2d.setColor(COLOR);    
 
    request.getSession().setAttribute(SESSION_CAPTCHA_VARIABLE, String.copyValueOf(cap));

    int x = 0; 
    int y = 0;

    for (int i=0; i<cap.length; i++) {
        x += 10 + (Math.abs(r.nextInt()) % 16);
        y = 20 + Math.abs(r.nextInt()) % 20;
        g2d.drawChars(cap, i, 1, x, y);
    }

    g2d.dispose();

    response.setContentType(CONTENT_TYPE);
    OutputStream os = response.getOutputStream();
    ImageIO.write(bufferedImage, IMAGE_TYPE, os);
    os.close();    
  } 


  protected void doGet(HttpServletRequest request, 
                       HttpServletResponse response)
                           throws ServletException, IOException {
      processRequest(request, response);
  } 

}