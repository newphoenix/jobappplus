package org.phoenix.utils;

import java.awt.image.BufferedImage;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Mode;

public class ImageUtil {

	private static final int MAIN_IMG_WIDTH = 160;
	private static final int MAIN_IMG_HEIGHT = 120;
	private static final int IMG_WIDTH = 640;
	private static final int IMG_HEIGHT = 480;
	private static final int LOGO_IMG_WIDTH = 240;
	private static final int LOGO_IMG_HEIGHT = 180;
	

	public static BufferedImage resizeMainImage(BufferedImage originalImage,
			int type) {

		if (originalImage.getWidth() <= MAIN_IMG_WIDTH && originalImage.getHeight() <= MAIN_IMG_HEIGHT) {
			return originalImage;
		}

		int new_height = MAIN_IMG_HEIGHT;
		int new_width = MAIN_IMG_WIDTH;

		if (originalImage.getWidth() > originalImage.getHeight()) {

			float ratio = (float) originalImage.getHeight()
					/ (float) originalImage.getWidth();
			new_height = (int) (MAIN_IMG_WIDTH * ratio);

		} else if (originalImage.getWidth() < originalImage.getHeight()) {

			float ratio = (float) originalImage.getHeight()
					/ (float) originalImage.getWidth();
			new_width = (int) (MAIN_IMG_HEIGHT * ratio);
		}

		BufferedImage resizedImage = Scalr.resize(originalImage,
				Mode.AUTOMATIC, new_width, new_height);

		return resizedImage;
	}

	public static BufferedImage resizeImage(BufferedImage originalImage,
			int type) {

		if (originalImage.getWidth() <= IMG_WIDTH
				&& originalImage.getHeight() <= IMG_HEIGHT) {
			return originalImage;
		}

		int new_height = IMG_HEIGHT;
		int new_width = IMG_WIDTH;

		if (originalImage.getWidth() > originalImage.getHeight()) {

			float ratio = (float) originalImage.getHeight()
					/ (float) originalImage.getWidth();
			new_height = (int) (IMG_WIDTH * ratio);

		} else if (originalImage.getWidth() < originalImage.getHeight()) {

			float ratio = (float) originalImage.getHeight()
					/ (float) originalImage.getWidth();
			new_width = (int) (IMG_HEIGHT * ratio);
		}

		BufferedImage resizedImage = Scalr.resize(originalImage,
				Mode.AUTOMATIC, new_width, new_height);

		return resizedImage;
	}
	
	public static BufferedImage resizeLogoImage(BufferedImage originalImage,
			int type) {

		if (originalImage.getWidth() <= LOGO_IMG_WIDTH
				&& originalImage.getHeight() <= LOGO_IMG_HEIGHT) {
			return originalImage;
		}

		int new_height = LOGO_IMG_HEIGHT;
		int new_width = LOGO_IMG_WIDTH;

		if (originalImage.getWidth() > originalImage.getHeight()) {

			float ratio = (float) originalImage.getHeight()
					/ (float) originalImage.getWidth();
			new_height = (int) (LOGO_IMG_WIDTH * ratio);

		} else if (originalImage.getWidth() < originalImage.getHeight()) {

			float ratio = (float) originalImage.getHeight()
					/ (float) originalImage.getWidth();
			new_width = (int) (LOGO_IMG_HEIGHT * ratio);
		}

		BufferedImage resizedImage = Scalr.resize(originalImage,
				Mode.AUTOMATIC, new_width, new_height);

		return resizedImage;
	}

}
