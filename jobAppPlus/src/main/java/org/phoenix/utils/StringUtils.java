package org.phoenix.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.phoenix.controller.BasicController;



public class StringUtils {

	private final static String CHAR_ENCODING = "UTF-8";

	public static String toUTF8(String str) {

		if (str == null)
			return null;

		try {
			return new String(str.getBytes(), CHAR_ENCODING);
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static String[] toUTF8(String[] str) {

		if (str == null || str.length == 0)
			return null;

		List<String> result = new ArrayList<String>(0);

		try {

			for (int i = 0; i < str.length; i++) {

				String s = new String(str[i].getBytes(), CHAR_ENCODING);
				result.add(s);
			}

			return result.toArray(new String[result.size()]);
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
	
	
	public static String generateCompanyCode(){		
		return  Long.toString(Math.abs(BasicController.R.nextLong()), 36)+"C"+Long.toString(Math.abs(BasicController.R.nextLong()), 36);
	}

	
	public static String generateUserCode(){		
		return  Long.toString(Math.abs(BasicController.R.nextLong()), 36)+"U"+Long.toString(Math.abs(BasicController.R.nextLong()), 36);
	}
	
}
