package org.phoenix.exception;

public class GenericException extends RuntimeException {

	private static final long serialVersionUID = 1874589658245L;
	private String customMsg;
	 
 
	public String getCustomMsg() {
		return customMsg;
	}

	public void setCustomMsg(String customMsg) {
		this.customMsg = customMsg;
	}

	public GenericException(String customMsg) {
		this.customMsg = customMsg;
	}
 
}