package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:44.056+0200")
@StaticMetamodel(Address.class)
public class Address_ {
	public static volatile SingularAttribute<Address, Integer> id;
	public static volatile SingularAttribute<Address, City> city;
	public static volatile SingularAttribute<Address, Region> region;
	public static volatile SingularAttribute<Address, String> addressCode;
	public static volatile SingularAttribute<Address, String> street;
	public static volatile SingularAttribute<Address, String> streetNo;
	public static volatile SingularAttribute<Address, String> zipCode;
	public static volatile SingularAttribute<Address, Integer> displayOrder;
	public static volatile SingularAttribute<Address, Integer> status;
	public static volatile SingularAttribute<Address, Date> created;
	public static volatile SingularAttribute<Address, String> createdBy;
	public static volatile SingularAttribute<Address, Date> modified;
	public static volatile SingularAttribute<Address, String> modifiedBy;
	public static volatile SetAttribute<Address, Company> companies;
}
