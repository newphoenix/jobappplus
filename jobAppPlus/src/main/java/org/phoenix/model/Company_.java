package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:33.149+0200")
@StaticMetamodel(Company.class)
public class Company_ {
	public static volatile SingularAttribute<Company, Integer> id;
	public static volatile SingularAttribute<Company, Category> category;
	public static volatile SingularAttribute<Company, CompanyType> companyType;
	public static volatile SingularAttribute<Company, User> user;
	public static volatile SingularAttribute<Company, String> companyCode;
	public static volatile SingularAttribute<Company, String> name;
	public static volatile SingularAttribute<Company, String> phoneNumber1;
	public static volatile SingularAttribute<Company, String> phoneNumber2;
	public static volatile SingularAttribute<Company, String> fax;
	public static volatile SingularAttribute<Company, String> web;
	public static volatile SingularAttribute<Company, String> email;
	public static volatile SingularAttribute<Company, String> contactTitle;
	public static volatile SingularAttribute<Company, String> contactName;
	public static volatile SingularAttribute<Company, Integer> contactPhone;
	public static volatile SingularAttribute<Company, Integer> contactFax;
	public static volatile SingularAttribute<Company, String> contactEmail;
	public static volatile SingularAttribute<Company, String> about;
	public static volatile SingularAttribute<Company, Integer> displayOrder;
	public static volatile SingularAttribute<Company, Integer> status;
	public static volatile SingularAttribute<Company, Date> created;
	public static volatile SingularAttribute<Company, String> createdBy;
	public static volatile SingularAttribute<Company, Date> modified;
	public static volatile SingularAttribute<Company, String> modifiedBy;
	public static volatile SetAttribute<Company, Picture> pictures;
	public static volatile SetAttribute<Company, Address> addresses;
	public static volatile SetAttribute<Company, Job> jobs;
}
