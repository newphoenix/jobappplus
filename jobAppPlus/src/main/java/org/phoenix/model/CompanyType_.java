package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:33.155+0200")
@StaticMetamodel(CompanyType.class)
public class CompanyType_ {
	public static volatile SingularAttribute<CompanyType, Integer> id;
	public static volatile SingularAttribute<CompanyType, String> name;
	public static volatile SingularAttribute<CompanyType, Integer> displayOrder;
	public static volatile SingularAttribute<CompanyType, Integer> status;
	public static volatile SingularAttribute<CompanyType, Date> created;
	public static volatile SingularAttribute<CompanyType, String> createdBy;
	public static volatile SingularAttribute<CompanyType, Date> modified;
	public static volatile SingularAttribute<CompanyType, String> modifiedBy;
	public static volatile SetAttribute<CompanyType, Company> companies;
}
