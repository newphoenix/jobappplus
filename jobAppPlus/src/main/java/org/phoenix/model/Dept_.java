package org.phoenix.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.016+0200")
@StaticMetamodel(Dept.class)
public class Dept_ {
	public static volatile SingularAttribute<Dept, Integer> deptno;
	public static volatile SingularAttribute<Dept, String> dname;
	public static volatile SingularAttribute<Dept, String> loc;
	public static volatile SetAttribute<Dept, Emp> emps;
}
