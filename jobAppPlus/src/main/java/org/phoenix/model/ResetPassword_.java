package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.039+0200")
@StaticMetamodel(ResetPassword.class)
public class ResetPassword_ {
	public static volatile SingularAttribute<ResetPassword, Integer> id;
	public static volatile SingularAttribute<ResetPassword, String> email;
	public static volatile SingularAttribute<ResetPassword, String> confirmationCode;
	public static volatile SingularAttribute<ResetPassword, Date> created;
	public static volatile SingularAttribute<ResetPassword, String> createdBy;
	public static volatile SingularAttribute<ResetPassword, Date> modified;
	public static volatile SingularAttribute<ResetPassword, String> modifiedBy;
}
