package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.033+0200")
@StaticMetamodel(Region.class)
public class Region_ {
	public static volatile SingularAttribute<Region, Integer> id;
	public static volatile SingularAttribute<Region, Country> country;
	public static volatile SingularAttribute<Region, String> name;
	public static volatile SingularAttribute<Region, Integer> displayOrder;
	public static volatile SingularAttribute<Region, Integer> status;
	public static volatile SingularAttribute<Region, String> shortName;
	public static volatile SingularAttribute<Region, String> code;
	public static volatile SingularAttribute<Region, Date> created;
	public static volatile SingularAttribute<Region, String> createdBy;
	public static volatile SingularAttribute<Region, Date> modified;
	public static volatile SingularAttribute<Region, String> modifiedBy;
	public static volatile SetAttribute<Region, Address> addresses;
	public static volatile SetAttribute<Region, Job> jobs;
	public static volatile SetAttribute<Region, City> cities;
}
