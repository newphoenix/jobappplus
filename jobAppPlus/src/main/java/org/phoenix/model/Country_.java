package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.013+0200")
@StaticMetamodel(Country.class)
public class Country_ {
	public static volatile SingularAttribute<Country, Integer> id;
	public static volatile SingularAttribute<Country, String> name;
	public static volatile SingularAttribute<Country, Integer> displayOrder;
	public static volatile SingularAttribute<Country, Integer> status;
	public static volatile SingularAttribute<Country, String> a2;
	public static volatile SingularAttribute<Country, String> a3;
	public static volatile SingularAttribute<Country, Integer> numericCode;
	public static volatile SingularAttribute<Country, Date> created;
	public static volatile SingularAttribute<Country, String> createdBy;
	public static volatile SingularAttribute<Country, Date> modified;
	public static volatile SingularAttribute<Country, String> modifiedBy;
	public static volatile SetAttribute<Country, Job> jobs;
	public static volatile SetAttribute<Country, Region> regions;
	public static volatile SetAttribute<Country, City> cities;
	public static volatile SetAttribute<Country, Registration> registrations;
	public static volatile SetAttribute<Country, User> users;
}
