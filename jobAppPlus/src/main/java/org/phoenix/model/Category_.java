package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:33.143+0200")
@StaticMetamodel(Category.class)
public class Category_ {
	public static volatile SingularAttribute<Category, Integer> id;
	public static volatile SingularAttribute<Category, String> name;
	public static volatile SingularAttribute<Category, Integer> displayOrder;
	public static volatile SingularAttribute<Category, Integer> status;
	public static volatile SingularAttribute<Category, Date> created;
	public static volatile SingularAttribute<Category, String> createdBy;
	public static volatile SingularAttribute<Category, Date> modified;
	public static volatile SingularAttribute<Category, String> modifiedBy;
	public static volatile SetAttribute<Category, Company> companies;
	public static volatile SetAttribute<Category, Job> jobs;
}
