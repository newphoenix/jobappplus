package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.041+0200")
@StaticMetamodel(User.class)
public class User_ {
	public static volatile SingularAttribute<User, Integer> id;
	public static volatile SingularAttribute<User, Country> country;
	public static volatile SingularAttribute<User, Gender> gender;
	public static volatile SingularAttribute<User, String> email;
	public static volatile SingularAttribute<User, String> userCode;
	public static volatile SingularAttribute<User, String> name;
	public static volatile SingularAttribute<User, String> surname;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, String> phoneNumber;
	public static volatile SingularAttribute<User, Date> birthday;
	public static volatile SingularAttribute<User, String> picture;
	public static volatile SingularAttribute<User, String> website;
	public static volatile SingularAttribute<User, Boolean> isCompany;
	public static volatile SingularAttribute<User, Date> lastTimeLog;
	public static volatile SingularAttribute<User, Boolean> enabled;
	public static volatile SingularAttribute<User, Date> created;
	public static volatile SingularAttribute<User, String> createdBy;
	public static volatile SingularAttribute<User, Date> modified;
	public static volatile SingularAttribute<User, String> modifiedBy;
	public static volatile SetAttribute<User, Authority> authorities;
	public static volatile SetAttribute<User, Company> companies;
}
