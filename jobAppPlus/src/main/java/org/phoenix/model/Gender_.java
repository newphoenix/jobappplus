package org.phoenix.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.022+0200")
@StaticMetamodel(Gender.class)
public class Gender_ {
	public static volatile SingularAttribute<Gender, Integer> id;
	public static volatile SingularAttribute<Gender, String> sex;
	public static volatile SetAttribute<Gender, User> users;
	public static volatile SetAttribute<Gender, Registration> registrations;
}
