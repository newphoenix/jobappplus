package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.010+0200")
@StaticMetamodel(Contact.class)
public class Contact_ {
	public static volatile SingularAttribute<Contact, Integer> id;
	public static volatile SingularAttribute<Contact, String> email;
	public static volatile SingularAttribute<Contact, String> subject;
	public static volatile SingularAttribute<Contact, String> message;
	public static volatile SingularAttribute<Contact, Boolean> read;
	public static volatile SingularAttribute<Contact, Date> created;
}
