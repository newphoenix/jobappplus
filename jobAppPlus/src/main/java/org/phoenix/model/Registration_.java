package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.036+0200")
@StaticMetamodel(Registration.class)
public class Registration_ {
	public static volatile SingularAttribute<Registration, Integer> id;
	public static volatile SingularAttribute<Registration, Country> country;
	public static volatile SingularAttribute<Registration, Gender> gender;
	public static volatile SingularAttribute<Registration, String> email;
	public static volatile SingularAttribute<Registration, String> name;
	public static volatile SingularAttribute<Registration, String> surname;
	public static volatile SingularAttribute<Registration, String> password;
	public static volatile SingularAttribute<Registration, Date> birthday;
	public static volatile SingularAttribute<Registration, String> confirmationCode;
	public static volatile SingularAttribute<Registration, String> sentMail;
	public static volatile SingularAttribute<Registration, Date> created;
	public static volatile SingularAttribute<Registration, String> createdBy;
	public static volatile SingularAttribute<Registration, Date> modified;
	public static volatile SingularAttribute<Registration, String> modifiedBy;
}
