package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.030+0200")
@StaticMetamodel(Picture.class)
public class Picture_ {
	public static volatile SingularAttribute<Picture, Integer> id;
	public static volatile SingularAttribute<Picture, Company> company;
	public static volatile SingularAttribute<Picture, String> name;
	public static volatile SingularAttribute<Picture, String> path;
	public static volatile SingularAttribute<Picture, Integer> size;
	public static volatile SingularAttribute<Picture, Boolean> status;
	public static volatile SingularAttribute<Picture, Date> created;
	public static volatile SingularAttribute<Picture, String> createdBy;
	public static volatile SingularAttribute<Picture, Date> modified;
	public static volatile SingularAttribute<Picture, String> modifiedBy;
}
