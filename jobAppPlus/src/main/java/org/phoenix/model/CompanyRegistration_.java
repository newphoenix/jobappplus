package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:33.152+0200")
@StaticMetamodel(CompanyRegistration.class)
public class CompanyRegistration_ {
	public static volatile SingularAttribute<CompanyRegistration, Integer> id;
	public static volatile SingularAttribute<CompanyRegistration, String> companyName;
	public static volatile SingularAttribute<CompanyRegistration, String> email;
	public static volatile SingularAttribute<CompanyRegistration, String> name;
	public static volatile SingularAttribute<CompanyRegistration, String> surname;
	public static volatile SingularAttribute<CompanyRegistration, String> phoneNumber1;
	public static volatile SingularAttribute<CompanyRegistration, String> password;
	public static volatile SingularAttribute<CompanyRegistration, Integer> countryId;
	public static volatile SingularAttribute<CompanyRegistration, Integer> companyTypeId;
	public static volatile SingularAttribute<CompanyRegistration, String> confirmationCode;
	public static volatile SingularAttribute<CompanyRegistration, String> sentMail;
	public static volatile SingularAttribute<CompanyRegistration, Date> created;
	public static volatile SingularAttribute<CompanyRegistration, String> createdBy;
	public static volatile SingularAttribute<CompanyRegistration, Date> modified;
	public static volatile SingularAttribute<CompanyRegistration, String> modifiedBy;
}
