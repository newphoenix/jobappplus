package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.026+0200")
@StaticMetamodel(Job.class)
public class Job_ {
	public static volatile SingularAttribute<Job, Integer> id;
	public static volatile SingularAttribute<Job, CareerLevel> careerLevel;
	public static volatile SingularAttribute<Job, Category> category;
	public static volatile SingularAttribute<Job, City> city;
	public static volatile SingularAttribute<Job, Company> company;
	public static volatile SingularAttribute<Job, Country> country;
	public static volatile SingularAttribute<Job, Region> region;
	public static volatile SingularAttribute<Job, String> jobCode;
	public static volatile SingularAttribute<Job, Boolean> isImage;
	public static volatile SingularAttribute<Job, String> imageLocation;
	public static volatile SingularAttribute<Job, String> title;
	public static volatile SingularAttribute<Job, String> description;
	public static volatile SingularAttribute<Job, String> responsibility;
	public static volatile SingularAttribute<Job, String> requirement;
	public static volatile SingularAttribute<Job, String> education;
	public static volatile SingularAttribute<Job, String> personalQuality;
	public static volatile SingularAttribute<Job, String> otherRequirement;
	public static volatile SingularAttribute<Job, String> degree;
	public static volatile SingularAttribute<Job, Integer> numberOfPositions;
	public static volatile SingularAttribute<Job, Date> startDate;
	public static volatile SingularAttribute<Job, String> positionType;
	public static volatile SingularAttribute<Job, Integer> yearsOfExperienceMin;
	public static volatile SingularAttribute<Job, Integer> yearsOfExperienceMax;
	public static volatile SingularAttribute<Job, Date> validFrom;
	public static volatile SingularAttribute<Job, Date> validTo;
	public static volatile SingularAttribute<Job, String> applyLink;
	public static volatile SingularAttribute<Job, Integer> displayOrder;
	public static volatile SingularAttribute<Job, Integer> status;
	public static volatile SingularAttribute<Job, Date> created;
	public static volatile SingularAttribute<Job, String> createdBy;
	public static volatile SingularAttribute<Job, Date> modified;
	public static volatile SingularAttribute<Job, String> modifiedBy;
}
