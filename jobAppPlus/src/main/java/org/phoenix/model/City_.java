package org.phoenix.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:33.146+0200")
@StaticMetamodel(City.class)
public class City_ {
	public static volatile SingularAttribute<City, Integer> id;
	public static volatile SingularAttribute<City, Country> country;
	public static volatile SingularAttribute<City, Region> region;
	public static volatile SingularAttribute<City, String> name;
	public static volatile SingularAttribute<City, String> code;
	public static volatile SingularAttribute<City, Integer> displayOrder;
	public static volatile SingularAttribute<City, BigDecimal> latitude;
	public static volatile SingularAttribute<City, BigDecimal> longitude;
	public static volatile SingularAttribute<City, String> timezone;
	public static volatile SingularAttribute<City, Date> created;
	public static volatile SingularAttribute<City, String> createdBy;
	public static volatile SingularAttribute<City, Date> modified;
	public static volatile SingularAttribute<City, String> modifiedBy;
	public static volatile SetAttribute<City, Address> addresses;
	public static volatile SetAttribute<City, Job> jobs;
}
