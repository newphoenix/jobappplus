package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:41:39.019+0200")
@StaticMetamodel(Emp.class)
public class Emp_ {
	public static volatile SingularAttribute<Emp, Integer> empno;
	public static volatile SingularAttribute<Emp, Dept> dept;
	public static volatile SingularAttribute<Emp, String> ename;
	public static volatile SingularAttribute<Emp, String> job;
	public static volatile SingularAttribute<Emp, Integer> mgr;
	public static volatile SingularAttribute<Emp, Date> hiredate;
	public static volatile SingularAttribute<Emp, Double> sal;
	public static volatile SingularAttribute<Emp, Double> comm;
}
