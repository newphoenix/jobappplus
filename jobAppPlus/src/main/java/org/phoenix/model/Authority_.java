package org.phoenix.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:44.069+0200")
@StaticMetamodel(Authority.class)
public class Authority_ {
	public static volatile SingularAttribute<Authority, AuthorityId> id;
	public static volatile SingularAttribute<Authority, User> user;
}
