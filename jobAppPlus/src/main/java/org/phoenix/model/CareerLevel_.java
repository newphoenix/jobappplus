package org.phoenix.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:33.141+0200")
@StaticMetamodel(CareerLevel.class)
public class CareerLevel_ {
	public static volatile SingularAttribute<CareerLevel, Integer> id;
	public static volatile SingularAttribute<CareerLevel, String> name;
	public static volatile SingularAttribute<CareerLevel, Integer> displayOrder;
	public static volatile SingularAttribute<CareerLevel, Integer> status;
	public static volatile SingularAttribute<CareerLevel, Date> created;
	public static volatile SingularAttribute<CareerLevel, String> createdBy;
	public static volatile SingularAttribute<CareerLevel, Date> modified;
	public static volatile SingularAttribute<CareerLevel, String> modifiedBy;
	public static volatile SetAttribute<CareerLevel, Job> jobs;
}
