package org.phoenix.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-14T16:45:44.072+0200")
@StaticMetamodel(AuthorityId.class)
public class AuthorityId_ {
	public static volatile SingularAttribute<AuthorityId, String> email;
	public static volatile SingularAttribute<AuthorityId, String> authority;
}
