package org.phoenix.dao;

import java.util.List;

import org.phoenix.model.Contact;

public interface IContact {

	void persist(Contact transientInstance);

	void remove(Contact persistentInstance);

	Contact merge(Contact detachedInstance);

	Contact findById(Integer id);
	
	public List<Contact> findByExample(Contact instance);

	public int insertMessage(String email, String subject, String body) throws Exception;

	public List<Contact> finaAllMessages(String onlyNotRead) throws Exception;

	public int deleteMsg(String msgId) throws Exception;

	public Contact findMsgById(String msgId) throws Exception;

	public int setMsgRead(String msgId) throws Exception;

}