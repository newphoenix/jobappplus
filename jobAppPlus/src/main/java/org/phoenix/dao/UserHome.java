package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.User;
import org.springframework.stereotype.Repository;

import net.glxn.qbe.QBE;

/**
 * Home object for domain model class User.
 * @see org.phoenix.model.User
 * @author Hibernate Tools
 */
@Repository
public class UserHome implements IUser {

	private static final Log log = LogFactory.getLog(UserHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IUser#persist(org.phoenix.model.User)
	 */
	@Override
	public void persist(User transientInstance) {
		log.debug("persisting User instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IUser#remove(org.phoenix.model.User)
	 */
	@Override
	public void remove(User persistentInstance) {
		log.debug("removing User instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IUser#merge(org.phoenix.model.User)
	 */
	@Override
	public User merge(User detachedInstance) {
		log.debug("merging User instance");
		try {
			User result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IUser#findById(java.lang.Integer)
	 */
	@Override
	public User findById(Integer id) {
		log.debug("getting User instance with id: " + id);
		try {
			User instance = entityManager.find(User.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	

	
	@Override
	public List<User> findByExample(User instance) {
		log.debug("finding User instance by example");
		try {
//			List<User> results = (List<User>) entityManager
//					.createCriteria("org.phoenix.model.User")
//					.add(create(instance)).list();
			
			List<User> results =  QBE.using(entityManager)
		        .query(User.class)
		        .by(instance)
		        .list();
			
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@Override
	public String checkIfEmailExists(String email) throws Exception {
		String namedQuery = "user.emailExist";

		try {
			return (String) entityManager
					.createNamedQuery(namedQuery,String.class)
					.setParameter("emailParam", email).getSingleResult();
		} catch (Exception re) {
			log.error("findByEmail failed", re);
			throw re;
		}
	}

	@Override
	public User findByEmail(String email) throws Exception {
		try {
//			User result = (User) entityManager.createCriteria("org.phoenix.model.User")
//					.add(Restrictions.eq("email", email)).uniqueResult();
			
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
			Root<User> c = criteriaQuery.from(User.class);

			criteriaQuery.select(c).where(builder.equal(c.get("email").as(String.class), email));

			Query query = entityManager.createQuery(criteriaQuery);
			User result = (User) query.getSingleResult();
			
			
			log.debug("findByEmail successful");
			return result;
		} catch (Exception re) {
			log.error("findByEmail failed", re);
			throw re;
		}
	}

	@Override
	public void attachDirty(User instance) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attachClean(User instance) {
		// TODO Auto-generated method stub
		
	}
}
