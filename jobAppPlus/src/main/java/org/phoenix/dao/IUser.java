package org.phoenix.dao;

import java.util.List;

import org.phoenix.model.User;

public interface IUser {

	public void persist(User transientInstance);

	public void attachDirty(User instance);

	public void attachClean(User instance);

	public void remove(User persistentInstance);

	public User merge(User detachedInstance);

	public User findById(java.lang.Integer id);

	public List<User> findByExample(User instance);

	public String checkIfEmailExists(String email) throws Exception;

	public User findByEmail(String email) throws Exception;

}