package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Region;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Region.
 * @see org.phoenix.model.Region
 * @author Hibernate Tools
 */
@Repository
public class RegionHome implements IRegion {

	private static final Log log = LogFactory.getLog(RegionHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IRegion#persist(org.phoenix.model.Region)
	 */
	@Override
	public void persist(Region transientInstance) {
		log.debug("persisting Region instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IRegion#remove(org.phoenix.model.Region)
	 */
	@Override
	public void remove(Region persistentInstance) {
		log.debug("removing Region instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IRegion#merge(org.phoenix.model.Region)
	 */
	@Override
	public Region merge(Region detachedInstance) {
		log.debug("merging Region instance");
		try {
			Region result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IRegion#findById(java.lang.Integer)
	 */
	@Override
	public Region findById(Integer id) {
		log.debug("getting Region instance with id: " + id);
		try {
			Region instance = entityManager.find(Region.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
