package org.phoenix.dao;

import org.phoenix.model.CareerLevel;

public interface ICareerLevel {

	void persist(CareerLevel transientInstance);

	void remove(CareerLevel persistentInstance);

	CareerLevel merge(CareerLevel detachedInstance);

	CareerLevel findById(Integer id);

}