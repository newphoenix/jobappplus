package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.CompanyRegistration;
import org.springframework.stereotype.Repository;

import net.glxn.qbe.QBE;

/**
 * Home object for domain model class CompanyRegistration.
 * @see org.phoenix.model.CompanyRegistration
 * @author Hibernate Tools
 */
@Repository
public class CompanyRegistrationHome implements ICompanyRegistration {

	private static final Log log = LogFactory.getLog(CompanyRegistrationHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompanyRegistration#persist(org.phoenix.model.CompanyRegistration)
	 */
	@Override
	public void persist(CompanyRegistration transientInstance) {
		log.debug("persisting CompanyRegistration instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompanyRegistration#remove(org.phoenix.model.CompanyRegistration)
	 */
	@Override
	public void remove(CompanyRegistration persistentInstance) {
		log.debug("removing CompanyRegistration instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompanyRegistration#merge(org.phoenix.model.CompanyRegistration)
	 */
	@Override
	public CompanyRegistration merge(CompanyRegistration detachedInstance) {
		log.debug("merging CompanyRegistration instance");
		try {
			CompanyRegistration result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompanyRegistration#findById(java.lang.Integer)
	 */
	@Override
	public CompanyRegistration findById(Integer id) {
		log.debug("getting CompanyRegistration instance with id: " + id);
		try {
			CompanyRegistration instance = entityManager.find(CompanyRegistration.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	
	@Override
	public List<CompanyRegistration> findByExample(CompanyRegistration instance) {
		log.debug("finding CompanyRegistration instance by example");
		try {
//			List<CompanyRegistration> results = (List<CompanyRegistration>) sessionFactory
//					.getCurrentSession().createCriteria("CompanyRegistration")
//					.add(create(instance)).list();
			
			List<CompanyRegistration> results =  QBE.using(entityManager)
			        .query(CompanyRegistration.class)
			        .by(instance)
			        .list();	
			
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@Override
	public String checkIfEmailExists(String email) throws Exception {
		String namedQuery = "CompanyRegistration.emailExistCompanyRegistrationTable";

		try {
			return (String) entityManager
					.createNamedQuery(namedQuery,String.class)
					.setParameter("emailRegParam", email).getSingleResult();
		} catch (Exception re) {
			log.error("checkIfEmailExists failed", re);
			throw re;
		}
	}

	@Override
	public CompanyRegistration findByConfirmationCode(String xcode)
			throws Exception {
		try {
//			CompanyRegistration result = (CompanyRegistration) sessionFactory
//					.getCurrentSession()
//					.createCriteria("org.phoenix.model.CompanyRegistration")
//					.add(Restrictions.eq("confirmationCode", xcode))
//					.uniqueResult();
//			
			
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<CompanyRegistration> criteriaQuery = builder.createQuery(CompanyRegistration.class);
			Root<CompanyRegistration> c = criteriaQuery.from(CompanyRegistration.class);

			criteriaQuery.select(c).where(builder.equal(c.get("confirmationCode").as(String.class), xcode));

			Query query = entityManager.createQuery(criteriaQuery);
			CompanyRegistration result = (CompanyRegistration) query.getSingleResult();

			return result;
		} catch (Exception re) {
			log.error("findByConfirmationCode failed", re);
			throw re;
		}
	}

	@Override
	public CompanyRegistration findByEmailAndConfirmation(String email,
			String confirmation) throws Exception {
		try {
//			CompanyRegistration result = (CompanyRegistration) sessionFactory
//					.getCurrentSession()
//					.createCriteria("org.phoenix.model.CompanyRegistration")
//					.add(Restrictions.and(Restrictions.eq("email", email),
//							Restrictions.eq("confirmationCode", confirmation)))
//					.uniqueResult();
			
			
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<CompanyRegistration> criteriaQuery = builder.createQuery(CompanyRegistration.class);
			Root<CompanyRegistration> c = criteriaQuery.from(CompanyRegistration.class);

			criteriaQuery.select(c).where(builder.equal(c.get("email").as(String.class), email),
					builder.equal(c.get("confirmationCode").as(String.class), confirmation));

			Query query = entityManager.createQuery(criteriaQuery);
			CompanyRegistration result = (CompanyRegistration) query.getSingleResult();

			return result;
		} catch (Exception re) {
			log.error("findByEmailAndConfirmation failed", re);
			throw re;
		}
	}

	@Override
	public Boolean findByCompanyName(String companyName) throws Exception {
		String sql = "SELECT count(name) from company where lower(name) = LOWER(:compName)";
	
		try{	
			BigInteger r =  (BigInteger) entityManager.createNativeQuery(sql).setParameter("compName", companyName).getSingleResult();
		    return (r.intValue() == 1)? true:false;
		    
		} catch (Exception re) {
			log.error("findByCompanyName", re);
			throw re;
		}	
	}
}
