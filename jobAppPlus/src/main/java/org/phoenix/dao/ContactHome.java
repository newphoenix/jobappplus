package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Contact;
import org.springframework.stereotype.Repository;

import net.glxn.qbe.QBE;

/**
 * Home object for domain model class Contact.
 * @see org.phoenix.model.Contact
 * @author Hibernate Tools
 */
@Repository
public class ContactHome implements IContact {

	private static final Log log = LogFactory.getLog(ContactHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IContact#persist(org.phoenix.model.Contact)
	 */
	@Override
	public void persist(Contact transientInstance) {
		log.debug("persisting Contact instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IContact#remove(org.phoenix.model.Contact)
	 */
	@Override
	public void remove(Contact persistentInstance) {
		log.debug("removing Contact instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IContact#merge(org.phoenix.model.Contact)
	 */
	@Override
	public Contact merge(Contact detachedInstance) {
		log.debug("merging Contact instance");
		try {
			Contact result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IContact#findById(java.lang.Integer)
	 */
	@Override
	public Contact findById(Integer id) {
		log.debug("getting Contact instance with id: " + id);
		try {
			Contact instance = entityManager.find(Contact.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see main.dao.IContact#findByExample(org.phoenix.model.Contact)
	 */

	@Override
	public List<Contact> findByExample(Contact instance) {
		log.debug("finding Contact instance by example");
		try {
//			List<Contact> results = (List<Contact>) sessionFactory
//					.getCurrentSession().createCriteria("org.phoenix.model.Contact")
//					.add(create(instance)).list();
//			
			List<Contact> results =  QBE.using(entityManager)
			        .query(Contact.class)
			        .by(instance)
			        .list();	
			
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@Override
	public int insertMessage(String email, String subject, String body)
			throws Exception {
		String sqlQuery = "INSERT INTO contact(email,`subject`,message,`read`) "
				+ "VALUES('"
				+ email
				+ "','"
				+ subject
				+ "','"
				+ body
				+ "',"
				+ 0 + ")";
		int result = -1;

		try {
			result = entityManager
					.createNativeQuery(sqlQuery).executeUpdate();

			return result;

		} catch (Exception re) {
			log.error("insertMessage", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Contact> finaAllMessages(String onlyNotRead) throws Exception {
		String sql = "select id,email,`subject`,`read`,created from contact	";

		// 0 : not read
		// 1 : all
		if ("0".equals(onlyNotRead)) {
			sql += " WHERE `read`  = 0  ";
		}

		sql += " order by created desc";

		try {
			List<Object[]> contactList = entityManager
					.createNativeQuery(sql).getResultList();

			if (contactList != null && !contactList.isEmpty()) {
				List<Contact> lst = new ArrayList<Contact>();
				Iterator<Object[]> itr = contactList.iterator();

				while (itr.hasNext()) {
					Object[] obj = itr.next();
					Contact contact = new Contact();
					contact.setId((Integer) obj[0]);
					contact.setEmail((String) obj[1]);
					contact.setSubject((String) obj[2]);
					contact.setRead((Boolean) obj[3]);
					contact.setCreated((Date) obj[4]);

					lst.add(contact);
				}

				return lst;
			}
		} catch (Exception re) {
			log.error("finaAllMessages", re);
			throw re;
		}

		return null;
	}

	@Override
	public int deleteMsg(String msgId) throws Exception {
		String sql = "Delete From contact where id = " + msgId;

		int result = -1;

		try {
			result = entityManager.createNativeQuery(sql)
					.executeUpdate();

			return result;

		} catch (Exception re) {
			log.error("deleteMsg", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Contact findMsgById(String msgId) throws Exception {
		String sql = "SELECT email,`subject`,message,created FROM contact Where id= "
				+ msgId;

		try {
			List<Object[]> contactLst = entityManager
					.createNativeQuery(sql).getResultList();

			if (contactLst != null && !contactLst.isEmpty()) {
				Object[] obj = contactLst.get(0);
				Contact contact = new Contact();
				contact.setEmail(((String) obj[0]).trim());
				contact.setSubject(((String) obj[1]).trim());
				contact.setMessage(((String) obj[2]).trim());
				contact.setCreated((Date) obj[3]);

				return contact;
			}

		} catch (Exception e) {
			log.error("findMsgById", e);
			throw e;
		}

		return null;
	}

	@Override
	public int setMsgRead(String msgId) throws Exception {
		String sql = "UPDATE contact set `read`=1 where id = " + msgId;

		int result = -1;

		try {
			result = entityManager.createNativeQuery(sql)
					.executeUpdate();

			return result;

		} catch (Exception re) {
			log.error("setMsgRead", re);
			throw re;
		}
	}
}
