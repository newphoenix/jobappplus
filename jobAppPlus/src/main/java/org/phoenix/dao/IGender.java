package org.phoenix.dao;

import org.phoenix.model.Gender;

public interface IGender {

	void persist(Gender transientInstance);

	void remove(Gender persistentInstance);

	Gender merge(Gender detachedInstance);

	Gender findById(Integer id);
	
	public void attachDirty(Gender instance);

}