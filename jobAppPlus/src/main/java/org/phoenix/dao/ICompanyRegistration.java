package org.phoenix.dao;

import java.util.List;

import org.phoenix.model.CompanyRegistration;

public interface ICompanyRegistration {

	void persist(CompanyRegistration transientInstance);

	void remove(CompanyRegistration persistentInstance);

	CompanyRegistration merge(CompanyRegistration detachedInstance);

	CompanyRegistration findById(Integer id);

	public List<CompanyRegistration> findByExample(CompanyRegistration instance);

	public String checkIfEmailExists(String email)  throws Exception;

	public CompanyRegistration findByConfirmationCode(String xcode) throws Exception;

	public CompanyRegistration findByEmailAndConfirmation(String email,	String confirmation)throws Exception;

	public Boolean findByCompanyName(String companyName) throws Exception;


}