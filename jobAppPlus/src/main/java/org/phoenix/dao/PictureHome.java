package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Picture;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Picture.
 * @see org.phoenix.model.Picture
 * @author Hibernate Tools
 */
@Repository
public class PictureHome implements IPicture {

	private static final Log log = LogFactory.getLog(PictureHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IPicture#persist(org.phoenix.model.Picture)
	 */
	@Override
	public void persist(Picture transientInstance) {
		log.debug("persisting Picture instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IPicture#remove(org.phoenix.model.Picture)
	 */
	@Override
	public void remove(Picture persistentInstance) {
		log.debug("removing Picture instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IPicture#merge(org.phoenix.model.Picture)
	 */
	@Override
	public Picture merge(Picture detachedInstance) {
		log.debug("merging Picture instance");
		try {
			Picture result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IPicture#findById(java.lang.Integer)
	 */
	@Override
	public Picture findById(Integer id) {
		log.debug("getting Picture instance with id: " + id);
		try {
			Picture instance = entityManager.find(Picture.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
