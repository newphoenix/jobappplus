package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.CompanyType;
import org.springframework.stereotype.Repository;

import net.glxn.qbe.QBE;

/**
 * Home object for domain model class CompanyType.
 * @see org.phoenix.model.CompanyType
 * @author Hibernate Tools
 */
@Repository
public class CompanyTypeHome implements ICompanyType {

	private static final Log log = LogFactory.getLog(CompanyTypeHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompanyType#persist(org.phoenix.model.CompanyType)
	 */
	@Override
	public void persist(CompanyType transientInstance) {
		log.debug("persisting CompanyType instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompanyType#remove(org.phoenix.model.CompanyType)
	 */
	@Override
	public void remove(CompanyType persistentInstance) {
		log.debug("removing CompanyType instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompanyType#merge(org.phoenix.model.CompanyType)
	 */
	@Override
	public CompanyType merge(CompanyType detachedInstance) {
		log.debug("merging CompanyType instance");
		try {
			CompanyType result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompanyType#findById(java.lang.Integer)
	 */
	@Override
	public CompanyType findById(Integer id) {
		log.debug("getting CompanyType instance with id: " + id);
		try {
			CompanyType instance = entityManager.find(CompanyType.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	

	@Override
	public List<CompanyType> findByExample(CompanyType instance) {
		log.debug("finding CompanyType instance by example");
		try {
//			List<CompanyType> results = (List<CompanyType>) sessionFactory
//					.getCurrentSession().createCriteria("org.phoenix.model.CompanyType")
//					.add(create(instance)).list();
//			
			List<CompanyType> results =  QBE.using(entityManager)
			        .query(CompanyType.class)
			        .by(instance)
			        .list();	
			
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Integer> readCompanyTypeName_Id() throws Exception {
		String sql = "SELECT m.name, m.id FROM company_type m order by display_order";

		try {

			List<Object[]> objList = entityManager.createNativeQuery(sql).getResultList();
			
			if (objList != null && (objList.size() > 0)) {
				
				Map<String, Integer> resultMap = new HashMap<String, Integer>();								
				Iterator<Object[]> itr = objList.iterator();

				while (itr.hasNext()) {
					Object[] objs = itr.next();
					resultMap.put((String) objs[0], (Integer) objs[1]);					
				}				
				return resultMap;
			}

		} catch (Exception ex) {
              log.error("readCompanyTypeName_Id", ex);
              throw ex;
		}

		return null;
	}
}
