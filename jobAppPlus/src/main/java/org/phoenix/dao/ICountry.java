package org.phoenix.dao;

import java.util.List;
import java.util.Map;

import org.phoenix.model.Country;

public interface ICountry {

	void persist(Country transientInstance);

	void remove(Country persistentInstance);

	Country merge(Country detachedInstance);

	Country findById(Integer id);
	
	public List<Country> findByExample(Country instance);

	public Country findByName(String country) throws Exception;

	public Map<String, Integer> readCountryName_Id() throws Exception;
	
	public void attachDirty(Country instance);

}