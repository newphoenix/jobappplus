package org.phoenix.dao;

import org.phoenix.model.Region;

public interface IRegion {

	void persist(Region transientInstance);

	void remove(Region persistentInstance);

	Region merge(Region detachedInstance);

	Region findById(Integer id);

}