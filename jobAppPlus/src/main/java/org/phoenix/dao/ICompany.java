package org.phoenix.dao;

import org.phoenix.model.Company;

public interface ICompany {

	void persist(Company transientInstance);

	void remove(Company persistentInstance);

	Company merge(Company detachedInstance);

	Company findById(Integer id);

}