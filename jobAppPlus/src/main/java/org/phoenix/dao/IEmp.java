package org.phoenix.dao;

import org.phoenix.model.Emp;

public interface IEmp {

	void persist(Emp transientInstance);

	void remove(Emp persistentInstance);

	Emp merge(Emp detachedInstance);

	Emp findById(int id);

}