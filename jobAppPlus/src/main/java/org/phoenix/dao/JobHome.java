package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Job;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Job.
 * @see org.phoenix.model.Job
 * @author Hibernate Tools
 */
@Repository
public class JobHome implements IJob {

	private static final Log log = LogFactory.getLog(JobHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IJob#persist(org.phoenix.model.Job)
	 */
	@Override
	public void persist(Job transientInstance) {
		log.debug("persisting Job instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IJob#remove(org.phoenix.model.Job)
	 */
	@Override
	public void remove(Job persistentInstance) {
		log.debug("removing Job instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IJob#merge(org.phoenix.model.Job)
	 */
	@Override
	public Job merge(Job detachedInstance) {
		log.debug("merging Job instance");
		try {
			Job result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IJob#findById(java.lang.Integer)
	 */
	@Override
	public Job findById(Integer id) {
		log.debug("getting Job instance with id: " + id);
		try {
			Job instance = entityManager.find(Job.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
