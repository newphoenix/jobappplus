package org.phoenix.dao;

import java.util.List;
import java.util.Map;

import org.phoenix.model.CompanyType;

public interface ICompanyType {

	void persist(CompanyType transientInstance);

	void remove(CompanyType persistentInstance);

	CompanyType merge(CompanyType detachedInstance);

	CompanyType findById(Integer id);
	

	public List<CompanyType> findByExample(CompanyType instance);

	public Map<String, Integer> readCompanyTypeName_Id() throws Exception;

}