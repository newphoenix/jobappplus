package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.CareerLevel;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class CareerLevel.
 * @see org.phoenix.model.CareerLevel
 * @author Hibernate Tools
 */
@Repository
public class CareerLevelHome implements ICareerLevel {

	private static final Log log = LogFactory.getLog(CareerLevelHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICareerLevel#persist(org.phoenix.model.CareerLevel)
	 */
	@Override
	public void persist(CareerLevel transientInstance) {
		log.debug("persisting CareerLevel instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICareerLevel#remove(org.phoenix.model.CareerLevel)
	 */
	@Override
	public void remove(CareerLevel persistentInstance) {
		log.debug("removing CareerLevel instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICareerLevel#merge(org.phoenix.model.CareerLevel)
	 */
	@Override
	public CareerLevel merge(CareerLevel detachedInstance) {
		log.debug("merging CareerLevel instance");
		try {
			CareerLevel result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICareerLevel#findById(java.lang.Integer)
	 */
	@Override
	public CareerLevel findById(Integer id) {
		log.debug("getting CareerLevel instance with id: " + id);
		try {
			CareerLevel instance = entityManager.find(CareerLevel.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
