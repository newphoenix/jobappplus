package org.phoenix.dao;

import org.phoenix.model.Category;

public interface ICategory {

	void persist(Category transientInstance);

	void remove(Category persistentInstance);

	Category merge(Category detachedInstance);

	Category findById(Integer id);

}