package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Emp;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Emp.
 * @see org.phoenix.model.Emp
 * @author Hibernate Tools
 */
@Repository
public class EmpHome implements IEmp {

	private static final Log log = LogFactory.getLog(EmpHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IEmp#persist(org.phoenix.model.Emp)
	 */
	@Override
	public void persist(Emp transientInstance) {
		log.debug("persisting Emp instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IEmp#remove(org.phoenix.model.Emp)
	 */
	@Override
	public void remove(Emp persistentInstance) {
		log.debug("removing Emp instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IEmp#merge(org.phoenix.model.Emp)
	 */
	@Override
	public Emp merge(Emp detachedInstance) {
		log.debug("merging Emp instance");
		try {
			Emp result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IEmp#findById(int)
	 */
	@Override
	public Emp findById(int id) {
		log.debug("getting Emp instance with id: " + id);
		try {
			Emp instance = entityManager.find(Emp.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
