package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Country;
import org.springframework.stereotype.Repository;

import net.glxn.qbe.QBE;

/**
 * Home object for domain model class Country.
 * @see org.phoenix.model.Country
 * @author Hibernate Tools
 */
@Repository
public class CountryHome implements ICountry {

	private static final Log log = LogFactory.getLog(CountryHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICountry#persist(org.phoenix.model.Country)
	 */
	@Override
	public void persist(Country transientInstance) {
		log.debug("persisting Country instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICountry#remove(org.phoenix.model.Country)
	 */
	@Override
	public void remove(Country persistentInstance) {
		log.debug("removing Country instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICountry#merge(org.phoenix.model.Country)
	 */
	@Override
	public Country merge(Country detachedInstance) {
		log.debug("merging Country instance");
		try {
			Country result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICountry#findById(java.lang.Integer)
	 */
	@Override
	public Country findById(Integer id) {
		log.debug("getting Country instance with id: " + id);
		try {
			Country instance = entityManager.find(Country.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see main.dao.ICountry#findByExample(org.phoenix.model.Country)
	 */

	@Override
	public List<Country> findByExample(Country instance) {
		log.debug("finding Country instance by example");
		try {
//			List<Country> results = (List<Country>) sessionFactory
//					.getCurrentSession().createCriteria("org.phoenix.model.Country")
//					.add(create(instance)).list();
//			
			List<Country> results =  QBE.using(entityManager)
			        .query(Country.class)
			        .by(instance)
			        .list();
			
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@Override
	public Country findByName(String countryName) throws Exception {
		String namedQuery = "country.countryByName";

		try {
			Country country = (Country) entityManager
					.createNamedQuery(namedQuery,Country.class)
					.setParameter("countryNameParam", countryName)
					.getSingleResult();

			return country;

		} catch (Exception re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Integer> readCountryName_Id() throws Exception {
		String sql = "SELECT m.name, m.id FROM country m order by display_order";

		try {

			List<Object[]> objList = entityManager.createNativeQuery(sql).getResultList();
			
			if (objList != null && (objList.size() > 0)) {
				
				Map<String, Integer> resultMap = new HashMap<String, Integer>();								
				Iterator<Object[]> itr = objList.iterator();

				while (itr.hasNext()) {
					Object[] objs = itr.next();
					resultMap.put((String) objs[0], (Integer) objs[1]);					
				}				
				return resultMap;
			}

		} catch (Exception ex) {
              log.error("readCountryName_Id", ex);
              throw ex;
		}

		return null;
	}

	@Override
	public void attachDirty(Country instance) {
		// TODO Auto-generated method stub
		
	}
}
