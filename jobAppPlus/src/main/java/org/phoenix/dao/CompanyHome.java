package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Company;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Company.
 * @see org.phoenix.model.Company
 * @author Hibernate Tools
 */
@Repository
public class CompanyHome implements ICompany {

	private static final Log log = LogFactory.getLog(CompanyHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompany#persist(org.phoenix.model.Company)
	 */
	@Override
	public void persist(Company transientInstance) {
		log.debug("persisting Company instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompany#remove(org.phoenix.model.Company)
	 */
	@Override
	public void remove(Company persistentInstance) {
		log.debug("removing Company instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompany#merge(org.phoenix.model.Company)
	 */
	@Override
	public Company merge(Company detachedInstance) {
		log.debug("merging Company instance");
		try {
			Company result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.ICompany#findById(java.lang.Integer)
	 */
	@Override
	public Company findById(Integer id) {
		log.debug("getting Company instance with id: " + id);
		try {
			Company instance = entityManager.find(Company.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
