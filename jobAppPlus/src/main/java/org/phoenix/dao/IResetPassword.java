package org.phoenix.dao;

import java.util.List;

import org.phoenix.model.ResetPassword;

public interface IResetPassword {

	void persist(ResetPassword transientInstance);

	void remove(ResetPassword persistentInstance);

	ResetPassword merge(ResetPassword detachedInstance);

	ResetPassword findById(Integer id);
	
	public List<ResetPassword> findByExample(ResetPassword instance);

	public String findEmailByConfirmationCode(String confirmationCode)  throws Exception;

	public void removeFromResetPassword(String email)  throws Exception;

}