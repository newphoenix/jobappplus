package org.phoenix.dao;

import org.phoenix.model.City;

public interface ICity {

	void persist(City transientInstance);

	void remove(City persistentInstance);

	City merge(City detachedInstance);

	City findById(Integer id);

}