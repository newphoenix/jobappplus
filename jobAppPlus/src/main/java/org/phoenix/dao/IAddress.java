package org.phoenix.dao;

import org.phoenix.model.Address;

public interface IAddress {

	void persist(Address transientInstance);

	void remove(Address persistentInstance);

	Address merge(Address detachedInstance);

	Address findById(Integer id);

}