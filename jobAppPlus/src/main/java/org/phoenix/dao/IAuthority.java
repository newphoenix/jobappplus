package org.phoenix.dao;

import org.phoenix.model.Authority;
import org.phoenix.model.AuthorityId;

public interface IAuthority {

	void persist(Authority transientInstance);

	void remove(Authority persistentInstance);

	Authority merge(Authority detachedInstance);

	Authority findById(AuthorityId id);

}