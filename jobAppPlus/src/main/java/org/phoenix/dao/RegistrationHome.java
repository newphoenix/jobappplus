package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Registration;
import org.springframework.stereotype.Repository;

import net.glxn.qbe.QBE;

/**
 * Home object for domain model class Registration.
 * @see org.phoenix.model.Registration
 * @author Hibernate Tools
 */
@Repository
public class RegistrationHome implements IRegistration {

	private static final Log log = LogFactory.getLog(RegistrationHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IRegistration#persist(org.phoenix.model.Registration)
	 */
	@Override
	public void persist(Registration transientInstance) {
		log.debug("persisting Registration instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IRegistration#remove(org.phoenix.model.Registration)
	 */
	@Override
	public void remove(Registration persistentInstance) {
		log.debug("removing Registration instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IRegistration#merge(org.phoenix.model.Registration)
	 */
	@Override
	public Registration merge(Registration detachedInstance) {
		log.debug("merging Registration instance");
		try {
			Registration result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IRegistration#findById(java.lang.Integer)
	 */
	@Override
	public Registration findById(Integer id) {
		log.debug("getting Registration instance with id: " + id);
		try {
			Registration instance = entityManager.find(Registration.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	/* (non-Javadoc)
	 * @see main.dao.IRegistration#findByExample(org.phoenix.model.Registration)
	 */

	@Override
	public List<Registration> findByExample(Registration instance) {
		log.debug("finding Registration instance by example");
		try {
//			List<Registration> results = (List<Registration>) entityManager
//					.getCurrentSession().createCriteria("org.phoenix.model.Registration")
//					.add(create(instance)).list();			
			
			List<Registration> results =  QBE.using(entityManager)
			        .query(Registration.class)
			        .by(instance)
			        .list();	
			
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@Override
	public String checkIfEmailExists(String email) throws Exception {
		String namedQuery = "Registration.emailExistCompanyRegistrationTable";

		try {
			return (String) entityManager
					.createNamedQuery(namedQuery,String.class)
					.setParameter("emailRegParam", email).getSingleResult();
		} catch (Exception re) {
			log.error("checkIfEmailExists failed", re);
			throw re;
		}
	}
	

	@Override
	public Registration findByEmailAndConfirmation(String email,
			String confirmation) throws Exception {
		try {
			
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Registration> criteriaQuery = builder.createQuery(Registration.class);
			Root<Registration> c = criteriaQuery.from(Registration.class);

			criteriaQuery.select(c).where(builder.equal(c.get("email").as(String.class), email),
					builder.equal(c.get("confirmationCode").as(String.class), confirmation));

			Query query = entityManager.createQuery(criteriaQuery);
			Registration result = (Registration) query.getSingleResult();

			return result;
		} catch (Exception re) {
			log.error("findByEmailAndConfirmation failed", re);
			throw re;
		}
	}

	@Override
	public Registration findByConfirmationCode(String xcode) throws Exception {
		try {
//			Registration result = (Registration) entityManager
//					.getCurrentSession()
//					.createCriteria("org.phoenix.model.Registration")
//					.add(Restrictions.eq("confirmationCode", xcode))
//					.uniqueResult();
			
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Registration> criteriaQuery = builder.createQuery(Registration.class);
			Root<Registration> c = criteriaQuery.from(Registration.class);

			criteriaQuery.select(c).where(builder.equal(c.get("confirmationCode").as(String.class), xcode));

			Query query = entityManager.createQuery(criteriaQuery);
			Registration result = (Registration) query.getSingleResult();

			return result;
		} catch (Exception re) {
			log.error("findByConfirmationCode failed", re);
			throw re;
		}
	}

	@Override
	public void attachDirty(Registration instance) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attachClean(Registration instance) {
		// TODO Auto-generated method stub
		
	}
}
