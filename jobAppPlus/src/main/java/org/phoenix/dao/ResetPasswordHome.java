package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.ResetPassword;
import org.springframework.stereotype.Repository;

import net.glxn.qbe.QBE;

/**
 * Home object for domain model class ResetPassword.
 * @see org.phoenix.model.ResetPassword
 * @author Hibernate Tools
 */
@Repository
public class ResetPasswordHome implements IResetPassword {

	private static final Log log = LogFactory.getLog(ResetPasswordHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IResetPassword#persist(org.phoenix.model.ResetPassword)
	 */
	@Override
	public void persist(ResetPassword transientInstance) {
		log.debug("persisting ResetPassword instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IResetPassword#remove(org.phoenix.model.ResetPassword)
	 */
	@Override
	public void remove(ResetPassword persistentInstance) {
		log.debug("removing ResetPassword instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IResetPassword#merge(org.phoenix.model.ResetPassword)
	 */
	@Override
	public ResetPassword merge(ResetPassword detachedInstance) {
		log.debug("merging ResetPassword instance");
		try {
			ResetPassword result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IResetPassword#findById(java.lang.Integer)
	 */
	@Override
	public ResetPassword findById(Integer id) {
		log.debug("getting ResetPassword instance with id: " + id);
		try {
			ResetPassword instance = entityManager.find(ResetPassword.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@Override
	public List<ResetPassword> findByExample(ResetPassword instance) {
		log.debug("finding ResetPassword instance by example");
		try {
//			List<ResetPassword> results = (List<ResetPassword>) sessionFactory
//					.getCurrentSession().createCriteria("org.phoenix.model.ResetPassword")
//					.add(create(instance)).list();
			
			List<ResetPassword> results =  QBE.using(entityManager)
			        .query(ResetPassword.class)
			        .by(instance)
			        .list();			
			
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@Override
	public String findEmailByConfirmationCode(String xcode)
			throws Exception {
		try {
			String email = (String) entityManager
					.createQuery(
							"SELECT email FROM org.phoenix.model.ResetPassword WHERE confirmationCode = :confCode")
					.setParameter("confCode", xcode).getSingleResult();
			return email;
		} catch (Exception re) {
			log.error("findEmailByConfirmationCode failed", re);
			throw re;
		}
	}

	@Override
	public void removeFromResetPassword(String email) throws Exception {
		try {
			 entityManager
					.createQuery("DELETE FROM org.phoenix.model.ResetPassword WHERE email = :emailaddress")
					.setParameter("emailaddress", email).executeUpdate();
		} catch (Exception re) {
			log.error("removeFromResetPassword failed", re);
			throw re;
		}
		
	}
}
