package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Gender;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Gender.
 * @see org.phoenix.model.Gender
 * @author Hibernate Tools
 */
@Repository
public class GenderHome implements IGender {

	private static final Log log = LogFactory.getLog(GenderHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IGender#persist(org.phoenix.model.Gender)
	 */
	@Override
	public void persist(Gender transientInstance) {
		log.debug("persisting Gender instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IGender#remove(org.phoenix.model.Gender)
	 */
	@Override
	public void remove(Gender persistentInstance) {
		log.debug("removing Gender instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IGender#merge(org.phoenix.model.Gender)
	 */
	@Override
	public Gender merge(Gender detachedInstance) {
		log.debug("merging Gender instance");
		try {
			Gender result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IGender#findById(java.lang.Integer)
	 */
	@Override
	public Gender findById(Integer id) {
		log.debug("getting Gender instance with id: " + id);
		try {
			Gender instance = entityManager.find(Gender.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@Override
	public void attachDirty(Gender instance) {
		// TODO Auto-generated method stub
		
	}
}
