package org.phoenix.dao;

import org.phoenix.model.Dept;

public interface IDept {

	void persist(Dept transientInstance);

	void remove(Dept persistentInstance);

	Dept merge(Dept detachedInstance);

	Dept findById(int id);

}