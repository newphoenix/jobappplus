package org.phoenix.dao;

import org.phoenix.model.Picture;

public interface IPicture {

	void persist(Picture transientInstance);

	void remove(Picture persistentInstance);

	Picture merge(Picture detachedInstance);

	Picture findById(Integer id);

}