package org.phoenix.dao;

import org.phoenix.model.Job;

public interface IJob {

	void persist(Job transientInstance);

	void remove(Job persistentInstance);

	Job merge(Job detachedInstance);

	Job findById(Integer id);

}