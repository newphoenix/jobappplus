package org.phoenix.dao;

import java.util.List;

import org.phoenix.model.Registration;

public interface IRegistration {

	public void persist(Registration transientInstance);

	public void attachDirty(Registration instance);

	public void attachClean(Registration instance);

	public void remove(Registration persistentInstance);

	public Registration merge(Registration detachedInstance);

	public Registration findById(java.lang.Integer id);

	public List<Registration> findByExample(Registration instance);

	public String checkIfEmailExists(String email) throws Exception;

	public Registration findByEmailAndConfirmation(String email,
			String confirmation) throws Exception;

	public Registration findByConfirmationCode(String xcode) throws Exception;

}