package org.phoenix.dao;
// Generated May 14, 2016 4:41:36 PM by Hibernate Tools 5.1.0.Alpha1


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Dept;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Dept.
 * @see org.phoenix.model.Dept
 * @author Hibernate Tools
 */
@Repository
public class DeptHome implements IDept {

	private static final Log log = LogFactory.getLog(DeptHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IDept#persist(org.phoenix.model.Dept)
	 */
	@Override
	public void persist(Dept transientInstance) {
		log.debug("persisting Dept instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IDept#remove(org.phoenix.model.Dept)
	 */
	@Override
	public void remove(Dept persistentInstance) {
		log.debug("removing Dept instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IDept#merge(org.phoenix.model.Dept)
	 */
	@Override
	public Dept merge(Dept detachedInstance) {
		log.debug("merging Dept instance");
		try {
			Dept result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.dao.IDept#findById(int)
	 */
	@Override
	public Dept findById(int id) {
		log.debug("getting Dept instance with id: " + id);
		try {
			Dept instance = entityManager.find(Dept.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
