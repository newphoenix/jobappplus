package org.phoenix.dto;

import org.hibernate.validator.constraints.Email;
//import org.hibernate.validator.constraints.NotEmpty;

public class ForgetPasswordForm {
	
	
	//@NotEmpty(message="Email field is mandatory.")
	@Email(message="Not correct email format")
	String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
