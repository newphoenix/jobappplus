package org.phoenix.service;

import java.security.MessageDigest;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.controller.BasicController;
import org.phoenix.dao.IAddress;
import org.phoenix.dao.IAuthority;
import org.phoenix.dao.ICareerLevel;
import org.phoenix.dao.ICategory;
import org.phoenix.dao.ICity;
import org.phoenix.dao.ICompany;
import org.phoenix.dao.ICompanyRegistration;
import org.phoenix.dao.ICompanyType;
import org.phoenix.dao.IContact;
import org.phoenix.dao.ICountry;
import org.phoenix.dao.IGender;
import org.phoenix.dao.IJob;
import org.phoenix.dao.IPicture;
import org.phoenix.dao.IRegion;
import org.phoenix.dao.IRegistration;
import org.phoenix.dao.IResetPassword;
import org.phoenix.dao.IUser;
import org.phoenix.utils.MailSend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.support.RequestContextUtils;

public class BasicService {

	 final private static String MSG ="OK";
	 final private static String LINK_COMPANY = "linkCompany";
	 final private static String REGISTRATION_TEXT ="registrationText";
	 final private static String SUBJECT = "subject";
	 final private static String LINK ="link";
	 final private static String LINK_FORGET_PASSWORD = "linkForgetPassword";
	 final private static String SUBJECT_FORGETPASSWORD = "subjectForgetPassword";
	 final private static String FORGETPASSWORD_TEXT = "forgetPasswordText";
		
		private static final Log log = LogFactory.getLog(BasicService.class);

		@Autowired
		IAddress addressDao;
		
		@Autowired
		IAuthority authorityDao;
		
		@Autowired
		ICareerLevel careerLevelDao;
		
		@Autowired
		ICategory categoryDao;
		
		@Autowired
		ICity cityDao;
		
		@Autowired
		ICompanyRegistration companyRegistrationDao;
		
		@Autowired
		ICompanyType companyTypeDao;
		
		@Autowired
		ICompany companyDao;
		
		@Autowired
		ICountry countryDao;
		
		@Autowired
		IGender genderDao;
		
		@Autowired
		IJob jobDao;
		
		@Autowired
		IPicture pictureDao;
		
		@Autowired
		IRegion regionDao;
		
		@Autowired
		IRegistration registrationDao;
		
		@Autowired
		IResetPassword resetPasswordDao;
		
		@Autowired
		IUser userDao;
		
		@Autowired
		IContact contactDao;
	

		public void setAddressDao(IAddress addressDao) {
			this.addressDao = addressDao;
		}

		public void setAuthorityDao(IAuthority authorityDao) {
			this.authorityDao = authorityDao;
		}

		public void setCareerLevelDao(ICareerLevel careerLevelDao) {
			this.careerLevelDao = careerLevelDao;
		}

		public void setCityDao(ICity cityDao) {
			this.cityDao = cityDao;
		}

		public void setCompanyDao(ICompany companyDao) {
			this.companyDao = companyDao;
		}

		public void setCountryDao(ICountry countryDao) {
			this.countryDao = countryDao;
		}

		public void setGenderDao(IGender genderDao) {
			this.genderDao = genderDao;
		}

		public void setJobDao(IJob jobDao) {
			this.jobDao = jobDao;
		}

		public void setPictureDao(IPicture pictureDao) {
			this.pictureDao = pictureDao;
		}

		public void setRegionDao(IRegion regionDao) {
			this.regionDao = regionDao;
		}

		public void setRegistrationDao(IRegistration registrationDao) {
			this.registrationDao = registrationDao;
		}

		public void setResetPasswordDao(IResetPassword resetPasswordDao) {
			this.resetPasswordDao = resetPasswordDao;
		}

		public void setUserDao(IUser userDao) {
			this.userDao = userDao;
		}
				
		public IResetPassword getResetPasswordDao() {
			return resetPasswordDao;
		}

		public void setContactDao(IContact contactDao) {
			this.contactDao = contactDao;
		}
		
		public void setCompanyRegistrationDao(
				ICompanyRegistration companyRegistrationDao) {
			this.companyRegistrationDao = companyRegistrationDao;
		}

		public void setCompanyTypeDao(ICompanyType companyTypeDao) {
			this.companyTypeDao = companyTypeDao;
		}
	
		public void setCategoryDao(ICategory categoryDao) {
			this.categoryDao = categoryDao;
		}

		public synchronized static String SEND_MAIL_MANDRILL(String to_,String to_name, String confirmationCode,
				String linkToPage,MessageSource messageLocale,HttpServletRequest request) {
			
			Locale locale = RequestContextUtils.getLocale(request);
			String msg = "MSG";
			
			String from_ = BasicController.MAIL_PROP.getProperty("from");
			String from_name = "JobFinder App";
			String subject_ = null;
			String message_ = null;
					
			try {
				
				// Now set the actual message
				if (LINK.equals(linkToPage)) {					
					subject_ = messageLocale.getMessage(SUBJECT,null,locale); //BasicController.PROP.getProperty("subject");	
					message_ = constructMessage(LINK,REGISTRATION_TEXT,confirmationCode,locale,messageLocale);		
					
				} else if (LINK_FORGET_PASSWORD.equals(linkToPage)) {
					subject_ = messageLocale.getMessage(SUBJECT_FORGETPASSWORD,null,locale);//BasicController.PROP.getProperty("subjectForgetPassword");			
					message_ = constructMessage(LINK_FORGET_PASSWORD,FORGETPASSWORD_TEXT,confirmationCode,locale,messageLocale);	
				
				}else if(LINK_COMPANY.equals(linkToPage)){
					subject_ = messageLocale.getMessage(SUBJECT,null,locale); //BasicController.PROP.getProperty("subject");	
					message_ = constructMessage(LINK_COMPANY,REGISTRATION_TEXT,confirmationCode,locale,messageLocale);		
							
				}				
				
				// Send message
				if(MailSend.send(from_, from_name, to_, to_name, subject_, message_) == 1){
				    msg = MSG;
				}
			} catch (Exception mex) {
				log.error("Sending mail via mandrill failed",mex);
				msg = "FAILED";
			}

			return msg;
		}
		
		public static String getHex(byte[] b) throws Exception {
			String result = "";
			for (int i = 0; i < b.length; i++) {
				result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
			}
			return result;
		}
		
		
		public static String GetCode() {
			
			Random r = BasicController.R;
			String code = (Long.toString(Math.abs(r.nextLong()), 36)
						+ Integer.toString(Math.abs(r.nextInt(899999)) + 100000) + Long
						.toString(Math.abs(r.nextLong()), 34)).toUpperCase()
						+ Integer.toString(Math.abs(r.nextInt(999999)) + 100000);
			
		    return code;
		}
		
		public static String hashPassword(String password) throws Exception
		{		
			Random randomGenerator = BasicController.R;
			double randomDouble = randomGenerator.nextDouble();
			MessageDigest digest256 = MessageDigest.getInstance("SHA-256");
			byte[] hash256 = digest256.digest((randomDouble + "$1001#._-*")
					.getBytes());
			String h256 = BasicService.getHex(hash256);
			String px = h256.substring(0, 32) + password.substring(0, 32)
					+ h256.substring(32, 64) + password.substring(32, 64);
			
			return px;
		}
		
		private static String constructMessage(String webLink,String text,String confirmationCode,Locale locale,MessageSource messageLocale){	
			// {0} : what for , {1} : web link, {2} : link 
			Object[] args = new Object[3];
			args[0] =  messageLocale.getMessage(text,null,locale);
			args[1] = BasicController.PROP.getProperty(webLink)+confirmationCode;;
			args[2] = messageLocale.getMessage("link",null,locale);;
			return MessageFormat.format("<html><body><h4>{0}</h4><a href=\"{1}\">{2}</a></body></html>",args);
			
		}

		
		
	
}
