package org.phoenix.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("UserService")
@Transactional
public class UserService extends BasicService{
	
private static final Log log = LogFactory.getLog(UserService.class);
	
	private static final String MSG = "OK";
	private static final String FAILED ="Failed";

	@Transactional(readOnly = true)
	public User getUserByEmail(String email) throws Exception {
		User result = null;

		try {
			result = userDao.findByEmail(email);
		} catch (Exception ex) {
			log.error("getUserByEmail",ex);
			throw ex;
		}

		return result;
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String save(User user) {
		try {
			userDao.persist(user);
		} catch (Exception ex) {
			log.error("save",ex);			
			return FAILED;
		}
		return MSG;
	}

}
