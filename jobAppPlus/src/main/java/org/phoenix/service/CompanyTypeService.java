package org.phoenix.service;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.CompanyType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("CompanyTypeService")
@Transactional
public class CompanyTypeService extends BasicService{

	private static final Log log = LogFactory.getLog(CompanyTypeService.class);
	
	@Transactional(readOnly = true)
	public Map<String, Integer> getCompanyTypeName_Id() throws Exception {
		try {
			return companyTypeDao.readCompanyTypeName_Id();
		} catch (Exception e) {
			log.error("getCompanyTypeName_Id", e);
			throw e;
		}
	}
	
	@Transactional(readOnly = true)
	public CompanyType findCompanyTypeById(int companyTypeId) throws Exception {
		try {
			return companyTypeDao.findById(companyTypeId);
		} catch (Exception e) {
			log.error("getCompanyTypeName_Id", e);
			throw e;
		}
	}

}
