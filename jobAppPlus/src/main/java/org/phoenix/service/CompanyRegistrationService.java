package org.phoenix.service;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.controller.DataLists;
import org.phoenix.model.Authority;
import org.phoenix.model.Company;
import org.phoenix.model.CompanyRegistration;
import org.phoenix.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service("CompanyRegistrationService")
@Transactional
public class CompanyRegistrationService extends BasicService{
	
	private static final Log log = LogFactory.getLog(CompanyRegistrationService.class);
	
	final private static String REGISTER = "register";
	final private static String USER = "user";
	final private static String OK = "OK";
	final private static String FAILED = "Failed";
	final private static String ADMIN = "admin";

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String addCompanyToRegistration(String email, String name,
			String surname, String password, String companyName, String companyType, String phoneNumber, String country, String confirmationCode) throws Exception {
		
		try {
			Integer countryId = DataLists.COUNTRY_NAME_ID.get(country);
			Integer companyTypeId = DataLists.COMPANYTYPE_NAME_ID.get(companyType); 
						
			CompanyRegistration companyRegistration = new CompanyRegistration();
			
			companyRegistration.setCountryId(countryId);
			companyRegistration.setCompanyName(companyName);
			companyRegistration.setPhoneNumber1(phoneNumber);
			companyRegistration.setCompanyTypeId(companyTypeId);			
			companyRegistration.setConfirmationCode(confirmationCode);
			companyRegistration.setCreated(new Date());
			companyRegistration.setCreatedBy(ADMIN);
			companyRegistration.setEmail(email);
			companyRegistration.setName(name);
			companyRegistration.setSurname(surname);

			String px = BasicService.hashPassword(password);
			companyRegistration.setPassword(px);		

			companyRegistrationDao.persist(companyRegistration);
			
			return OK;

		} catch (Exception e) {
			log.error("addCompanyToRegistration", e);
			throw e;
		}
		
		
	}

	@Transactional(readOnly = true)
	public Object checkIfEmailExists(String email, String registerDao) throws Exception {
		String result = null;
		try {

			if (REGISTER.equals(registerDao))
				result = companyRegistrationDao.checkIfEmailExists(email);
			else if (USER.equals(registerDao))
				result = userDao.checkIfEmailExists(email);

			return result;
		} catch (Exception e) {
			log.error("checkIfEmailExists", e);
			throw e;
		}
		
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public void updateRegistrationMailSent(String sent, String email,
			String confirmation) throws Exception {
		try {
			CompanyRegistration companyRegistration = companyRegistrationDao
					.findByEmailAndConfirmation(email, confirmation);
			companyRegistration.setSentMail(sent);
			companyRegistrationDao.merge(companyRegistration);

		} catch (Exception e) {
			log.error("updateRegistrationMailSent", e);
			throw e;
		}
		
	}
	
	@Transactional(readOnly = true)
	public CompanyRegistration findByConfirmationCode(String xcode) throws Exception {
		try {

			return companyRegistrationDao.findByConfirmationCode(xcode);

		} catch (Exception e) {
			log.error("findByConfirmationCode", e);
			throw e;
		}
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String saveUserAuthCompany(User user,Authority auth,Company company) {
		try {
			userDao.persist(user);
			authorityDao.persist(auth);
			companyDao.persist(company);
		} catch (Exception ex) {
			log.error("saveUserAuthCompany", ex);
			return FAILED;
		}
		return OK;
	}
	
	
	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String saveUser(User user) {
		try {
			userDao.persist(user);
		} catch (Exception ex) {
			log.error("saveUser", ex);
			return FAILED;
		}
		return OK;
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String saveAuth(Authority auth) {
		try {
			authorityDao.persist(auth);
		} catch (Exception ex) {
			log.error("saveAuth", ex);
			return FAILED;
		}
		return OK;
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String saveCompany(Company company) {
		try {
			companyDao.persist(company);
		} catch (Exception ex) {
			log.error("saveCompany", ex);
			return FAILED;
		}
		return OK;
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public void remove(CompanyRegistration regCompany) throws Exception {
		try {
			companyRegistrationDao.remove(regCompany);
		} catch (Exception e) {
			log.error("remove", e);
			throw e;
		}
		
	}

	@Transactional(readOnly = true)
	public Boolean checkIfCompanyExists(String companyName) throws Exception {
		return companyRegistrationDao.findByCompanyName(companyName);
	}	

}
