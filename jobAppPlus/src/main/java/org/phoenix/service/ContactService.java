package org.phoenix.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Contact;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("ContactService")
@Transactional
public class ContactService extends BasicService {
	
	private static final Log log = LogFactory.getLog(ContactService.class);

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public int addMessage(String email, String subject, String body)
			throws Exception {
		int result = -1;

		try {
			result = contactDao.insertMessage(email, subject, body);
		} catch (Exception ex) {
			log.error("addMessage", ex);
			throw ex;
		}

		return result;
	}

	public List<Contact> getAllMessages(String onlyNotRead) throws Exception {

		List<Contact> msgs = new ArrayList<Contact>();
		
		try {
			msgs = contactDao.finaAllMessages(onlyNotRead);
		} catch (Exception ex) {
			log.error("getAllMessages", ex);
			throw ex;
		}
		
		return msgs;
	}

	public Contact getMessageById(String msgId) throws Exception {

		Contact msg = new Contact(); 
		
		try {
			  msg  = contactDao.findMsgById(msgId);
			  
			  if(contactDao.setMsgRead(msgId) != -1);{
				  msg.setRead(true);
			  }
			  
		} catch (Exception ex) {
			log.error("getMessageById", ex);
			throw ex;
		}
		
		return msg;
	}
	
	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public int removeMessageById(String msgId) throws Exception{
		
		int result = -1;
		
		try {
			  result =  contactDao.deleteMsg(msgId);
		} catch (Exception ex) {
			log.error("removeMessageById", ex);
			throw ex;
		}
		
		return result;
	}

}
