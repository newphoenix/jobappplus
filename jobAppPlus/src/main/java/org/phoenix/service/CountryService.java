package org.phoenix.service;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Country;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("CountryService")
@Transactional
public class CountryService extends BasicService{

	private static final Log log = LogFactory.getLog(CountryService.class);
	
	@Transactional(readOnly = true)
	public Map<String, Integer> getCountryName_Id()  throws Exception{
		try {
			return countryDao.readCountryName_Id();
		} catch (Exception e) {
			log.error("getCountryName_Id", e);
			throw e;
		}
	}

	@Transactional(readOnly = true)
	public Country findCountryById(int countryId) throws Exception {
		try {
			return countryDao.findById(countryId);
		} catch (Exception e) {
			log.error("findCountryById", e);
			throw e;
		}
	}
	

}
