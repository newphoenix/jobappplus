package org.phoenix.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Gender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("GenderService")
@Transactional
public class GenderService extends BasicService{
	private static final Log log = LogFactory.getLog(GenderService.class);

	@Transactional(readOnly = true)
	public Gender findGenderById(int i) throws Exception {
		try {
			return genderDao.findById(i);
		} catch (Exception e) {
			log.error("findGenderById", e);
			throw e;
		}
	}

}
