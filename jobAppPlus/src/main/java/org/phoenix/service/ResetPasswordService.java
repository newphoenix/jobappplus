package org.phoenix.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.ResetPassword;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("ResetPasswordService")
@Transactional
public class ResetPasswordService extends BasicService{
	
	private static final Log log = LogFactory.getLog(ResetPasswordService.class);

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public void saveResetPassword(ResetPassword resetPassword) throws Exception {
		try {
			resetPasswordDao.persist(resetPassword);
		} catch (Exception ex) {
			log.error("saveResetPassword",ex);
			throw ex;
		}
		
	}

	@Transactional(readOnly = true)
	public String getEmailByConfirmationCode(String confirmationCode)  throws Exception{
		try {
			return resetPasswordDao.findEmailByConfirmationCode(confirmationCode);
		} catch (Exception ex) {
			log.error("getEmailByConfirmationCode",ex);
			throw ex;
		}

	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public void removeFromResetPassword(String email)  throws Exception{
		try {
			resetPasswordDao.removeFromResetPassword(email);
		} catch (Exception ex) {
			log.error("removeFromResetPassword",ex);
			throw ex;
		}
		
	}

}
