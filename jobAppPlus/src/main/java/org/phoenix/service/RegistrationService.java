package org.phoenix.service;

import java.text.DateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Authority;
import org.phoenix.model.Country;
import org.phoenix.model.Gender;
import org.phoenix.model.Registration;
import org.phoenix.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("RegistrationService")
@Transactional
public class RegistrationService extends BasicService {

	private static final Log log = LogFactory.getLog(RegistrationService.class);

	final private static String REGISTER = "register";
	final private static String USER = "user";
	final private static String OK = "OK";
	final private static String FAILED = "Failed";
	final private static String ADMIN = "admin";

	@Transactional(readOnly = true)
	public Object checkIfEmailExists(String email, String daoName)
			throws Exception {
		String result = null;

		try {

			if (REGISTER.equals(daoName))
				result = registrationDao.checkIfEmailExists(email);
			else if (USER.equals(daoName))
				result = userDao.checkIfEmailExists(email);

			return result;
		} catch (Exception e) {
			log.error("checkIfEmailExists", e);
			throw e;
		}
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String addUserToRegistration(String email, String name,
			String surname, String password, String day, String month,
			String year, String gender, String country, String confirmationCode)
			throws Exception {
		try {

			Country countryObj = countryDao.findByName(country);

			Integer genderId = new Integer(2);
			if ("male".equals(gender))
				genderId = 1;

			Gender genderObj = genderDao.findById(genderId);

			Registration registration = new Registration();
			registration.setCountry(countryObj);
			registration.setGender(genderObj);
			registration.setConfirmationCode(confirmationCode);
			registration.setCreated(new Date());
			registration.setCreatedBy(ADMIN);
			registration.setEmail(email);
			registration.setName(name);
			registration.setSurname(surname);

			String px = BasicService.hashPassword(password);

			registration.setPassword(px);

			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			Date birthday = df.parse(month.substring(0, 3) + " " + day + ", "
					+ year);
			registration.setBirthday(birthday);

			countryObj.getRegistrations().add(registration);
			genderObj.getRegistrations().add(registration);

			registrationDao.persist(registration);
			genderDao.attachDirty(genderObj);
			countryDao.attachDirty(countryObj);

			return OK;

		} catch (Exception e) {
			log.error("addUserToRegistration", e);
			throw e;
		}

	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public void updateRegistrationMailSent(String sent, String email,
			String confirmation) throws Exception {
		try {
			Registration registration = registrationDao
					.findByEmailAndConfirmation(email, confirmation);
			registration.setSentMail(sent);
			registrationDao.merge(registration);

		} catch (Exception e) {
			log.error("updateRegistrationMailSent", e);
			throw e;
		}

	}

	@Transactional(readOnly = true)
	public Registration findByConfirmationCode(String xcode) throws Exception {
		try {

			return registrationDao.findByConfirmationCode(xcode);

		} catch (Exception e) {
			log.error("findByConfirmationCode", e);
			throw e;
		}
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String save(Authority auth) throws Exception {

		try {
			authorityDao.persist(auth);
		} catch (Exception ex) {
			log.error("save method2", ex);
			return FAILED;
		}
		return OK;
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public String save(User user) throws Exception {
		try {
			userDao.persist(user);
		} catch (Exception ex) {
			log.error("save method1", ex);
			return FAILED;
		}
		return OK;
	}

	@Transactional(rollbackFor = Exception.class, readOnly = false, timeout = 30, propagation = Propagation.SUPPORTS, isolation = Isolation.DEFAULT)
	public void remove(Registration reg) throws Exception {
		try {
			registrationDao.remove(reg);

		} catch (Exception e) {
			log.error("remove", e);
			throw e;
		}
	}

	

}
