package org.phoenix.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.model.Category;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("CategoryService")
@Transactional
public class CategoryService extends BasicService{
	private static final Log log = LogFactory.getLog(CategoryService.class);

	@Transactional(readOnly = true)
	public Category findCategoryById(int i) throws Exception {
		try {
			return categoryDao.findById(i);
		} catch (Exception e) {
			log.error("findCategoryById", e);
			throw e;
		}
	}

}
