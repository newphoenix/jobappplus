package org.phoenix.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Controller
@RequestMapping("/userLang")
public class AnonymousUserLocale {

	@Autowired
	SessionLocaleResolver sessionLocaleResolver;

	private static String From_Register_Page = "r";
	private static String From_COMPANY_Page = "rc";
	private final static String LOGIN_PAGE = "redirect:/login.jsp";
	private final static String REGISTER_PAGE = "redirect:/register.jsp";
	private final static String COMPANY_PAGE = "redirect:/company.jsp";
	private final static String EMPTY = "";

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String userlanguage(HttpServletRequest request,
			HttpServletResponse response) {

		String language = (String) request.getParameter("lang");
		String from = (String) request.getParameter("p");
		String redirectTo = LOGIN_PAGE;
		Locale loc = null;

		if (language != null && !EMPTY.equals(language)) {
			loc = new Locale(language);
		}

		if (loc != null) {
			response.setLocale(loc);
			sessionLocaleResolver.setLocale(request, response, loc);
			sessionLocaleResolver.resolveLocale(request);
			request.getSession().setAttribute("local", loc);
		}

		if (From_Register_Page.equals(from)) {
			redirectTo = REGISTER_PAGE;
		}else if (From_COMPANY_Page.equals(from)){
			redirectTo = COMPANY_PAGE;
		}

		return redirectTo;
	}

}
