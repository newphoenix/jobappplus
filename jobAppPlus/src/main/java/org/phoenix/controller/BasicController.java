package org.phoenix.controller;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.PixelGrabber;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

//import main.data.lists.CurrencyValues;


import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.exception.GenericException;
import org.phoenix.service.AddressService;
import org.phoenix.service.AuthorityService;
import org.phoenix.service.CareerLevelService;
import org.phoenix.service.CategoryService;
import org.phoenix.service.CityService;
import org.phoenix.service.CompanyRegistrationService;
import org.phoenix.service.CompanyService;
import org.phoenix.service.CompanyTypeService;
import org.phoenix.service.ContactService;
import org.phoenix.service.CountryService;
import org.phoenix.service.GenderService;
import org.phoenix.service.JobService;
import org.phoenix.service.PictureService;
import org.phoenix.service.RegionService;
import org.phoenix.service.RegistrationService;
import org.phoenix.service.ResetPasswordService;
import org.phoenix.service.UserService;
import org.phoenix.utils.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
//import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.support.RequestContextUtils;
//import org.springframework.web.servlet.LocaleResolver;
//import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.google.gson.Gson;

public class BasicController {

	// RequestContextUtils.getLocale(request)

	private static final Log log = LogFactory.getLog(BasicController.class);

	public static final String DEFAULT_PRICE = "";
	public static final String MALE = "male";
	public static final String FEMALE = "female";
	public static final String JPG = "jpg";
	public static final String PNG = "png";
	public static final String DEFAULT_IMAGE_NAME = "d.png";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static String PHOTO_DIRECTORY;
	public static String SHOP_LOGO_DIRECTORY;
	public static SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
	public static SimpleDateFormat MonthFormat = new SimpleDateFormat("MMMM");
	public static final String ROLE_ADMIN = "ROLE_admin";
	public static final String JSON_NULL = "null";
	public static final int DEFAULT_STATIC_POSITION = 11;
	public static final String DEFAULT_CURRENCY = "KM";
	public static final String ANONYMOUS_USER = "anonymousUser";

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static Pattern pattern = Pattern.compile(EMAIL_PATTERN);;
	private static Matcher matcher;
	// ------- application variables
	public static String SHOW_AD_SECTION = "showAdSection";
	public static String APPLICATION_SERVER_ADDRESS = "applicationServerAddress";
	// -------

	public final static DateFormat DATE_FORMAT_BIRTH_DATE = new SimpleDateFormat(
			"dd/MM/yyyy");

	public static final Properties PROP = new Properties();
	public static final Properties MAIL_PROP = new Properties();

	public static final String PROP_FILE_NAME = "app_config.properties";
	public static final String MAIL_PROP_FILE_NAME = "email.properties";

	public static final Random R = new Random();

	public static final Gson GSON = new Gson();

	// public static final String MAIN_IMAGE_NAME = "main.jpg";
	private static final String MAIN_IMAGE_JPG = "main.jpg";
	private static final String MAIN_IMAGE_PNG = "main.png";

	private static final int[] RGB_MASKS = {0xFF0000, 0xFF00, 0xFF};
	private static final ColorModel RGB_OPAQUE = new DirectColorModel(32, RGB_MASKS[0], RGB_MASKS[1], RGB_MASKS[2]);
	
	
	@Autowired
	private MessageSource messageSource;

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Resource(name = "AddressService")
	protected AddressService addressService;

	@Resource(name = "AuthorityService")
	protected AuthorityService authorityService;	
	
	@Resource(name = "CareerLevelService")
	protected CareerLevelService careerLevelService;
	
	@Resource(name = "CategoryService")
	protected CategoryService categoryService;
	
	@Resource(name = "CityService")
	protected CityService cityService;
	
	@Resource(name = "CompanyService")
	protected CompanyService companyService;
	
	@Resource(name = "CountryService")
	protected CountryService countryService;
	
	@Resource(name = "GenderService")
	protected GenderService genderService;
	
	@Resource(name = "JobService")
	protected JobService jobService;
	
	@Resource(name = "PictureService")
	protected PictureService pictureService;
	
	@Resource(name = "RegionService")
	protected RegionService regionService;
	
	@Resource(name = "RegistrationService")
	protected RegistrationService registrationService;
	
	@Resource(name = "ResetPasswordService")
	protected ResetPasswordService resetPasswordService;

	@Resource(name = "UserService")
	protected UserService userService;
	
	@Resource(name = "ContactService")
	protected ContactService contactService;
	
	@Resource(name = "CompanyRegistrationService")
	protected CompanyRegistrationService companyRegistrationService;
	
	@Resource(name = "CompanyTypeService")
	protected CompanyTypeService companyTypeService;
	
	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}


	public void setAuthorityService(AuthorityService authorityService) {
		this.authorityService = authorityService;
	}


	public void setCareerLevelService(CareerLevelService careerLevelService) {
		this.careerLevelService = careerLevelService;
	}


	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}


	public void setCityService(CityService cityService) {
		this.cityService = cityService;
	}


	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}


	public void setCountryService(CountryService countryService) {
		this.countryService = countryService;
	}


	public void setGenderService(GenderService genderService) {
		this.genderService = genderService;
	}


	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}


	public void setPictureService(PictureService pictureService) {
		this.pictureService = pictureService;
	}


	public void setRegionService(RegionService regionService) {
		this.regionService = regionService;
	}


	public void setRegistrationService(RegistrationService registrationService) {
		this.registrationService = registrationService;
	}


	public void setResetPasswordService(ResetPasswordService resetPasswordService) {
		this.resetPasswordService = resetPasswordService;
	}


	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public void setContactService(ContactService contactService) {
		this.contactService = contactService;
	}
	
	public void setCompanyRegistrationService(
			CompanyRegistrationService companyRegistrationService) {
		this.companyRegistrationService = companyRegistrationService;
	}


	public void setCompanyTypeService(CompanyTypeService companyTypeService) {
		this.companyTypeService = companyTypeService;
	}


	@PostConstruct
	public void onStartUp() throws Exception {

		applicationProperties(); // this call goes first
		checkAndFillDataLists();// this call goes second
		
	}

	
	private void checkAndFillDataLists() throws Exception {
		try {

			if (DataLists.COMPANYTYPE_NAME_ID == null) {
				DataLists.COMPANYTYPE_NAME_ID = companyTypeService.getCompanyTypeName_Id();				
				}
			
			if (DataLists.COUNTRY_NAME_ID == null) {
				DataLists.COUNTRY_NAME_ID = countryService.getCountryName_Id();	
			}
			
			if (DataLists.GENDER_LIST == null) {
				DataLists.GENDER_LIST = new HashMap<String, String>(2);
				DataLists.GENDER_LIST.put("male", "Male");
				DataLists.GENDER_LIST.put("female", "Female");
			}
			
			} catch (Exception e) {
				log.error("checkAndFillDataLists", e);
				throw e;
			}
		
	}


	private Map<String, String> getMapWithoutDefault(
			Map<String, String> cityMap, String defaultOption) {

		SortedMap<String, String> result = new TreeMap<String, String>();

		Set<String> keys = cityMap.keySet();
		Iterator<String> it = keys.iterator();

		while (it.hasNext()) {
			String type = it.next();

			if (!defaultOption.equals(type))
				result.put(type, type);
		}

		return result;
	}

	public static void applicationProperties() throws Exception {

		if (PROP.isEmpty()) {

			try {

				PROP.load(Thread.currentThread().getContextClassLoader()
						.getResourceAsStream(PROP_FILE_NAME));
			} catch (Exception ex) {
				log.error("Error loading properties file", ex);
				throw ex;
			}
		}

	}

	public static void mailProperties() throws Exception {
		if (MAIL_PROP.isEmpty()) {

			try {

				MAIL_PROP.load(Thread.currentThread().getContextClassLoader()
						.getResourceAsStream(MAIL_PROP_FILE_NAME));
			} catch (Exception ex) {
				log.error("Error loading mailProperties file", ex);
				throw ex;
			}
		}

	}

	private Object[] getSortedSet(Map<String, Integer> map) {

		if (map == null) {
			return null;
		}

		Object[] arrayOfObjects = map.keySet().toArray();
		Arrays.sort(arrayOfObjects);
		return arrayOfObjects;
	}

	private Object[] getObjectArray(List<String> lst, String filter) {

		if (lst == null) {
			return null;
		}

		lst.remove(filter); // remove the default option and add it in the html

		Object[] arrayOfObjects = lst.toArray(new String[lst.size()]);
		return arrayOfObjects;
	}

	private Map<String, String> getKeyKeyMap(Map<String, Integer> map) {

		SortedMap<String, String> result = new TreeMap<String, String>();

		Set<String> keys = map.keySet();
		Iterator<String> it = keys.iterator();

		while (it.hasNext()) {
			String type = it.next();
			result.put(type, type);
		}

		return result;
	}

	public Map<String, String> getKeyKeyMapi18n(Map<String, Integer> map,
			Locale locale) {

		SortedMap<String, String> result = new TreeMap<String, String>();

		Set<String> keys = map.keySet();
		Iterator<String> it = keys.iterator();

		while (it.hasNext()) {
			String type = it.next();
			result.put(type, messageSource.getMessage(type, null, locale));
		}

		return result;
	}

	public Map<String, String> geti18nGendar(Map<String, String> map,
			Locale locale) {

		SortedMap<String, String> result = new TreeMap<String, String>();

		Set<String> keys = map.keySet();
		Iterator<String> it = keys.iterator();

		while (it.hasNext()) {
			String type = it.next();
			result.put(type, messageSource.getMessage(type, null, locale));
		}

		return result;
	}

	protected String uniqueFolderName() {
		return Long.toString(Math.abs(BasicController.R.nextLong()), 36)
				+ Math.abs(BasicController.R.nextInt(19999));
	}

	protected String uniqueFileName(MultipartFile photoFile) {
		String fileExtension = photoFile.getOriginalFilename().substring(
				photoFile.getOriginalFilename().lastIndexOf("."),
				photoFile.getOriginalFilename().length());
		String fileName = Long.toString(Math.abs(R.nextLong()), 36)
				+ Math.abs(R.nextInt(19999));
		return fileName + fileExtension;
	}

	protected void resizeImage(MultipartFile multipartFile,
			String pathToPhotorDirectory) {		 	

		try {
			BufferedImage in =  readUploadedImage(multipartFile);
			
			if (in != null) {
				int type = in.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : in
						.getType();
				BufferedImage resizeImageJpgPng = ImageUtil.resizeImage(in,
						type);

				String PhotoType = JPG;
				if (multipartFile.getContentType().toLowerCase()
						.contains("/" + PNG)) { PhotoType = PNG; }

				ImageIO.write(resizeImageJpgPng, PhotoType, new File(
						pathToPhotorDirectory));
			}
		} catch (Exception ex) {
			log.error("resizeImage", ex);
			throw new GenericException("Error please try later");
		}

	}

	protected BufferedImage readUploadedImage(MultipartFile multipartFile) {

		BufferedImage bi = null;

		try {
			bi = ImageIO.read(multipartFile.getInputStream());
		} catch (Exception e) {
			try {
				byte[] bArray = multipartFile.getBytes();
				Image img = Toolkit.getDefaultToolkit().createImage(bArray);
				PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
				pg.grabPixels();
				int width = pg.getWidth(), height = pg.getHeight();

				DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(),
						pg.getWidth() * pg.getHeight());
				WritableRaster raster = Raster.createPackedRaster(buffer,
						width, height, width, RGB_MASKS, null);
				bi = new BufferedImage(RGB_OPAQUE, raster, false, null);
			} catch (Exception xe) {
				log.error("readUploadedImage", xe);
			}
		}

		return bi;
	}

	public void initPhotoDirectory() {
		if (PHOTO_DIRECTORY == null) {
			PHOTO_DIRECTORY = BasicController.PROP.getProperty("imageFolder");
		}

		if (SHOP_LOGO_DIRECTORY == null) {
			SHOP_LOGO_DIRECTORY = BasicController.PROP
					.getProperty("shopImageFolder");
		}
	}

	protected void setShowAdSectionApplicationVariable(
			HttpServletRequest request) {
		if (request.getServletContext().getAttribute(SHOW_AD_SECTION) == null) {
			request.getServletContext().setAttribute(SHOW_AD_SECTION,
					BasicController.PROP.getProperty(SHOW_AD_SECTION));
		}
	}

	protected void setShowAdSectionApplicationVariable(
			HttpServletRequest request, boolean value) {
		request.getServletContext().setAttribute(SHOW_AD_SECTION, value);
	}

	public static void setAPPLICATION_SERVER_ADDRESS(HttpServletRequest request) {

		if (request.getServletContext()
				.getAttribute(APPLICATION_SERVER_ADDRESS) == null) {
			request.getServletContext().setAttribute(
					APPLICATION_SERVER_ADDRESS,
					BasicController.PROP
							.getProperty(APPLICATION_SERVER_ADDRESS));
		}
	}
	
	public void initData_Properties(HttpServletRequest request)
			throws Exception {
		applicationProperties(); // this call goes first
		mailProperties();
		checkAndFillDataLists();// this call goes second
		setShowAdSectionApplicationVariable(request);
		setAPPLICATION_SERVER_ADDRESS(request);
		initPhotoDirectory();
		//CurrencyValues.initCurrencyPrices();
	}

	public static void setAPPLICATION_SERVER_ADDRESS(
			HttpServletRequest request, String value) {
		request.getServletContext().setAttribute(APPLICATION_SERVER_ADDRESS,
				value);
	}

	
	protected String makeMainImage(MultipartFile multipartFile,
			String pathToPhotorDirectory) {

		if (multipartFile == null || multipartFile.getSize() == 0) {
			return pathToPhotorDirectory + File.separator
					+ BasicController.DEFAULT_IMAGE_NAME;
		}

		String path = PHOTO_DIRECTORY + File.separator + DEFAULT_IMAGE_NAME;

		try {
			BufferedImage in = readUploadedImage(multipartFile);
			int type = in.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : in
					.getType();
			BufferedImage resizeImageJpg = ImageUtil.resizeMainImage(in, type);

			String PhotoType = JPG;
			String mainImage = MAIN_IMAGE_JPG;
			if (multipartFile.getContentType().contains("/"+PNG)) {
				PhotoType = PNG;
				mainImage = MAIN_IMAGE_PNG;
			}

			path = pathToPhotorDirectory + File.separator + mainImage;
			ImageIO.write(resizeImageJpg, PhotoType, new File(path));
		} catch (Exception ex) {
			log.error("makeMainImage", ex);
		}

		return path;
	}

	public synchronized static boolean validateEmail(String email) {
		matcher = pattern.matcher(email);
		return matcher.matches();
	}


	/*
	 * Authentication auth =
	 * SecurityContextHolder.getContext().getAuthentication();
	 */
	public static boolean userHasAuthority(String role,
			Collection<? extends GrantedAuthority> collection) {

		for (GrantedAuthority grantedAuthority : collection) {
			if (role.equals(grantedAuthority.getAuthority())) {
				return true;
			}
		}

		return false;
	}

}
