package org.phoenix.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.exception.GenericException;
import org.phoenix.model.Authority;
import org.phoenix.model.AuthorityId;
import org.phoenix.model.Category;
import org.phoenix.model.Company;
import org.phoenix.model.CompanyRegistration;
import org.phoenix.model.CompanyType;
import org.phoenix.model.Country;
import org.phoenix.model.Gender;
import org.phoenix.model.Registration;
import org.phoenix.model.User;
import org.phoenix.service.BasicService;
import org.phoenix.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Controller
@RequestMapping("/register")
public class RegisterController extends BasicController{

	private static final Log log = LogFactory.getLog(RegisterController.class);

	private static String STATUS = "Register Successfully, Activation Email sent";
	final private static String CAPTCHA_MESSAGE = "Please enter symbols correctly";
	final private static String REGISTERED_SUCCESSFULLY = "Registered";
	final private static String COMPANY_NAME_EXIST = "Company_Exist";
	final private static String OK = "OK";
	final private static String LINK = "link";
	final private static String LINK_COMPANY = "linkCompany";
	final private static String ROLE_USER = "ROLE_user";
	final private static String ROLE_COMPANY = "ROLE_company";
	final private static String REGISTER_DAO = "register";
	final private static String USER_DAO = "user";
	final private static String CAPTHCHA = "captcha";
	final private static String Y ="Y";
	final private static String N ="N";
	final private static String ADMIN = "admin";
	
	@Autowired
	private SessionLocaleResolver localeResolver;
	
	@Autowired
	private MessageSource messageLocale;	
		
	public void setMessageLocale(MessageSource messageLocale) {
		this.messageLocale = messageLocale;
	}

	@RequestMapping(value = "/p", method = RequestMethod.POST)
	public @ResponseBody
	String register(@RequestParam("email") String email,
			@RequestParam("name") String name,
			@RequestParam("surname") String surname,
			@RequestParam("password") String password,
			@RequestParam("month") String month,
			@RequestParam("day") String day, @RequestParam("year") String year,
			@RequestParam("gender") String gender,
			@RequestParam("country") String country,
			@RequestParam("code") String code, HttpServletRequest request) {

		try {
			initData_Properties(request);

			String confirmation;
			String captcha = (String) request.getSession().getAttribute(
					CAPTHCHA);

			if (code == null || captcha == null || !code.equals(captcha))
				return CAPTCHA_MESSAGE;

			if ((registrationService.checkIfEmailExists(email, REGISTER_DAO) != null)
					|| (registrationService.checkIfEmailExists(email, USER_DAO) != null)) {
				return REGISTERED_SUCCESSFULLY;
			}

			confirmation = BasicService.GetCode();

			try {
				registrationService.addUserToRegistration(email, name, surname,
						password, day, month, year, gender, country,
						confirmation);
				
				if (OK.equals(BasicService.SEND_MAIL_MANDRILL(email, name+surname, confirmation, LINK,messageLocale,request))) {
					registrationService.updateRegistrationMailSent(Y, email,
							confirmation);
				} else {
					registrationService.updateRegistrationMailSent(N, email,
							confirmation);
				}

			} catch (Exception e) {				
				log.error("regisrtration", e);
				STATUS = "ERROR2 " + e.getMessage();
			}

			return STATUS;

		} catch (Exception e) {
			log.error("register", e);
			throw new GenericException(
					BasicController.PROP.getProperty("jobApp.genericError"));
		}
	}

	@RequestMapping(value = "/confirm")
	public ModelAndView confirmation(@RequestParam("code") String xcode) {
		ModelAndView mav = new ModelAndView("registerConfirmation");

		try {
			Registration reg = registrationService.findByConfirmationCode(xcode);

			if (reg != null) {

				AuthorityId authId = new AuthorityId(reg.getEmail(), ROLE_USER);
				User user = new User();

				Authority auth = new Authority();
				auth.setId(authId);
				auth.setUser(user);

				Set<Authority> authSet = new HashSet<Authority>();
				authSet.add(auth);

				user.setAuthorities(authSet);
				user.setCountry(reg.getCountry());
				user.setGender(reg.getGender());
				user.setBirthday(reg.getBirthday());
				user.setCreatedBy(reg.getCreatedBy());
				user.setEmail(reg.getEmail());
				user.setPassword(reg.getPassword());
				user.setName(reg.getName());
				user.setSurname(reg.getSurname());
				user.setEnabled(true);
				user.setCreated(new Date());
				user.setUserCode(StringUtils.generateUserCode());				

				String a = registrationService.save(user);
				String b = registrationService.save(auth);

				if (!OK.equals(a) || !OK.equals(b)) {
					xcode = "ERROR";
				} else {
					registrationService.remove(reg);
				}

			} else {
				throw new GenericException("Confirmation is not Correct");
			}

			mav.addObject("code", xcode);

			return mav;

		} catch (Exception e) {
			log.error("confirmation", e);
			throw new GenericException(
					BasicController.PROP.getProperty("jobApp.genericError"));
		}

	}
	
	
	//------------------------ Register Company
	
	@RequestMapping(value = "/cr", method = RequestMethod.POST)
	public @ResponseBody
	String registerCompany_(@RequestParam("email") String email,
			@RequestParam("name") String name,
			@RequestParam("surname") String surname,
			@RequestParam("password") String password,
			@RequestParam("company") String companyName,
			@RequestParam("companyType") String companyType,
			@RequestParam("phone") String phoneNumber,			
			@RequestParam("country") String country,
			@RequestParam("code") String code, HttpServletRequest request) {

		try {
			initData_Properties(request);

			String confirmation;
			String captcha = (String) request.getSession().getAttribute(CAPTHCHA);

			if (code == null || captcha == null || !code.equals(captcha))
				return CAPTCHA_MESSAGE;

			if ((companyRegistrationService.checkIfEmailExists(email, REGISTER_DAO) != null)
					|| (companyRegistrationService.checkIfEmailExists(email, USER_DAO) != null)) {
				return REGISTERED_SUCCESSFULLY;
			}
			
			
			if(companyRegistrationService.checkIfCompanyExists(companyName)){
				return COMPANY_NAME_EXIST;
			}

			confirmation = BasicService.GetCode();

			try {
				companyRegistrationService.addCompanyToRegistration(email, name, surname,
						password, companyName, companyType, phoneNumber, country,
						confirmation);
				
				if (OK.equals(BasicService.SEND_MAIL_MANDRILL(email, name+surname, confirmation, LINK_COMPANY,messageLocale,request))) {
					companyRegistrationService.updateRegistrationMailSent(Y, email,
							confirmation);
				} else {
					companyRegistrationService.updateRegistrationMailSent(N, email,
							confirmation);
				}

			} catch (Exception e) {				
				log.error("company Registration Error", e);
				STATUS = "ERROR2 " + e.getMessage();
			}

			return STATUS;
		} catch (Exception e) {
			log.error("company register", e);
			throw new GenericException(
					BasicController.PROP.getProperty("jobApp.genericError"));
		}
	}
	
	@RequestMapping(value = "/confirmCompany")
	public ModelAndView confirmationCompany_(@RequestParam("code") String xcode) {
		ModelAndView mav = new ModelAndView("registerConfirmation");

		try {
			CompanyRegistration regCompany = companyRegistrationService.findByConfirmationCode(xcode);

			if (regCompany != null) {
				
				Country country = countryService.findCountryById(regCompany.getCountryId());
				CompanyType companyType = companyTypeService.findCompanyTypeById(regCompany.getCompanyTypeId());
				Category category = categoryService.findCategoryById(120);// default 120
				Gender gender = genderService.findGenderById(1); // default 1
				
				AuthorityId authId = new AuthorityId(regCompany.getEmail(), ROLE_COMPANY);
				User user = new User();

				Authority auth = new Authority();
				auth.setId(authId);
				auth.setUser(user);

				Set<Authority> authSet = new HashSet<Authority>();
				authSet.add(auth);
				
				Company company = constructCompany(regCompany,user,companyType,category);
				Set<Company> companySet = new HashSet<Company>();
				companySet.add(company);

				constructUser(regCompany, user ,authSet,country,gender,companySet);					
				
				String a = companyRegistrationService.saveUserAuthCompany(user,auth,company);

				if (!OK.equals(a)) {
					xcode = "ERROR";
				} else {
					companyRegistrationService.remove(regCompany);
				}

			} else {
				throw new GenericException("Confirmation is not Correct");
			}

			mav.addObject("code", xcode);
			return mav;

		} catch (Exception e) {
			log.error("confirmation", e);
			throw new GenericException(
					BasicController.PROP.getProperty("jobApp.genericError"));
		}
	}

	private void constructUser(CompanyRegistration regCompany, User user,
			Set<Authority> authSet,Country country,Gender gender,Set<Company> companySet) {			
		user.setAuthorities(authSet);
		user.setCountry(country);
		user.setGender(gender);
		user.setBirthday(new Date());
		user.setCreatedBy(regCompany.getCreatedBy());
		user.setEmail(regCompany.getEmail());
		user.setPassword(regCompany.getPassword());
		user.setName(regCompany.getName());
		user.setSurname(regCompany.getSurname());
		user.setEnabled(true);
		user.setCreated(new Date());
		user.setUserCode(StringUtils.generateUserCode());
		user.setCompanies(companySet);
		user.setIsCompany(true);
	}

	private Company constructCompany(CompanyRegistration regCompany,User user,CompanyType companyType,Category category) {
		
		Company company = new Company();
		company.setCompanyCode(StringUtils.generateCompanyCode());
		company.setName(regCompany.getCompanyName());
		company.setPhoneNumber1(regCompany.getPhoneNumber1());
		company.setEmail(regCompany.getEmail());
		company.setDisplayOrder(50000);
		company.setStatus(1);
		company.setCreated(new Date());
		company.setCreatedBy(ADMIN);
		
		company.setUser(user);
		company.setCategory(category);		
		company.setCompanyType(companyType);	
		
		return company;
	}
	
	//-----------------------

}
