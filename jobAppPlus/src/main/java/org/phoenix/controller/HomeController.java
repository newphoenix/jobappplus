package org.phoenix.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.exception.GenericException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController extends BasicController{
	
	private static final Log log = LogFactory.getLog(HomeController.class);	
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView firstPage(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("home"); // name of jsp file

	try{
		return mav;
	}catch(Exception e){
		log.error("home", e);
		throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
	}
	
	}

}
