package org.phoenix.controller;

import java.security.Principal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.dto.CompanyDataBean;
import org.phoenix.dto.UserDataBean;
import org.phoenix.exception.GenericException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.RequestContextUtils;


@Controller
@RequestMapping("/profile")
public class ProfileController extends BasicController {

	private static final Log log = LogFactory.getLog(ProfileController.class);
	
	@RequestMapping(value = "/data", method = RequestMethod.GET)
	public String getUserData(Map<String, Object> map,Model model,Principal princ,HttpServletRequest request) {
		
		try{
			initData_Properties(request);
		
		UserDataBean udb = getUserData(princ.getName());
		CompanyDataBean cdb = getCompanyData(udb.getId());
		udb.setId("0");
		
		model.addAttribute("profileData", udb);
				
		map.put("genderList", geti18nGendar(DataLists.GENDER_LIST,RequestContextUtils.getLocale(request)));
					
		return "profilePage";
		}catch(Exception e){
			log.error("getUserData", e);
			throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
		}
	}

	private CompanyDataBean getCompanyData(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	private UserDataBean getUserData(String name) {
		// TODO Auto-generated method stub
		return null;
	}
}
