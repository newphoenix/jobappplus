package org.phoenix.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/o")
public class OtherController extends BasicController {

	private static final Log log = LogFactory.getLog(OtherController.class);

	@RequestMapping(value = "/sm")
	public String siteMap() {
		try {
			return "siteMap";
		} catch (Exception e) {
			log.error("siteMap", e);
		}

		return null;

	}

	@RequestMapping(value = "/about")
	public String about() {
		try {
			return "about";
		} catch (Exception e) {
			log.error("about", e);
		}

		return null;
	}

}
