package org.phoenix.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.exception.GenericException;
import org.phoenix.model.Contact;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;




@Controller
@RequestMapping("/Contact")
public class ContactController extends BasicController{
	
	private static final Log log = LogFactory.getLog(ContactController.class);
	private static final String ON = "on";
	private static final String EMPTY = "";
	private static final int EMAIL_LENGTH = 256;
	private static final int SUBJECT_LENGTH = 256;
	private static final int MESSAGE_LENGTH = 4096;
	
	@RequestMapping(value = "/")
	public String getContactView() {		
		try{		
		     return "contact";
		}catch(Exception e){
			log.error("getContactView", e);
			throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
		}
	}
		
	@RequestMapping(value = "/msg" , produces = "application/json;charset=UTF-8")
	public @ResponseBody String addMessage_(@RequestParam("e") String email,
			@RequestParam("s") String subject,
			@RequestParam("m") String body){
		
		int i = 0;
		try{			
			 if(validateContactData(email,subject,body)){	
				  email= trimString(email,EMAIL_LENGTH);
				  subject= trimString(subject,SUBJECT_LENGTH);
				  body= trimString(body,MESSAGE_LENGTH);
			      i = contactService.addMessage(email,subject,body);
			      }
			 
		}catch(Exception e){
			log.error("addMessage_", e);
			throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
		}
		
		String result = "nok";
		if(i == 1){
			result = GSON.toJson("ok");
		}		
		return result;		
	}
	
	
	private String trimString(String str, int maxLength) {
		
		if(str.length() > maxLength){
			return str.substring(0, maxLength);
		}else{
			return str;
		}
	}

	private boolean validateContactData(String email, String subject,
			String body) {
	
		if(EMPTY.equals(subject.trim())){
			return false;
		}
		
		if(EMPTY.equals(body.trim())){
			return false;
		}
		
		if(EMPTY.equals(email.trim())){
			return false;
		}
				
		return validateEmail(email.trim());
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView viewMessages_(@RequestParam("f") String onlyNotRead){
		
		ModelAndView mav = new ModelAndView("contactMessages");
		
		try{		
			 List<Contact> msgLst = contactService.getAllMessages(onlyNotRead);
			 mav.addObject("msgLst", msgLst);
		 
			return mav;
		
		}catch(Exception e){
			log.error("viewMessages_", e);
			throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
		}
		
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.POST)
	public ModelAndView viewMessages_(HttpServletRequest request){
		
		ModelAndView mav = new ModelAndView("contactMessages");
		
		try{	
			
			String onlyNotRead = ON.equals(request.getParameter("cb"))? "0":"1";			
			
			 List<Contact> msgLst = contactService.getAllMessages(onlyNotRead);
			 mav.addObject("msgLst", msgLst);
		 
			return mav;
		
		}catch(Exception e){
			log.error("viewMessages_", e);
			throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
		}
		
	}
	
	
	
	@RequestMapping(value = "/readMsg", produces = "application/json;charset=UTF-8")
	public @ResponseBody
	String readMessage_(@RequestParam("i") String msgId) {

		Contact msg = null;

		try {

			msg = contactService.getMessageById(msgId);

		} catch (Exception e) {
			log.error("readMessage_", e);
			throw new GenericException(
					BasicController.PROP.getProperty("jobApp.genericError"));
		}

		return GSON.toJson(msg);

	}
		
	/*@RequestMapping(value = "/remove", produces = "application/json;charset=UTF-8")
	public @ResponseBody
	String removeMessage_(@RequestParam("i") String msgId) {

		try {

			int i = contactService.removeMessageById(msgId);

		} catch (Exception e) {
			log.error("removeMessage_", e);
			throw new GenericException(
					BasicController.PROP.getProperty("jobApp.genericError"));
		}

		return null;
	}*/

}
