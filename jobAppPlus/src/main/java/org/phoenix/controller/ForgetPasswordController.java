package org.phoenix.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.dto.ForgetPasswordForm;
import org.phoenix.dto.ResetPasswordForm;
import org.phoenix.exception.GenericException;
import org.phoenix.model.ResetPassword;
import org.phoenix.model.User;
import org.phoenix.service.BasicService;
import org.phoenix.validation.ForgetPasswordFormValid;
import org.phoenix.validation.ResetPasswordFormValid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Controller
@RequestMapping("/fpp")
public class ForgetPasswordController extends BasicController {

	private static final Log log = LogFactory
			.getLog(ForgetPasswordController.class);

	@Autowired
	ForgetPasswordFormValid validator;

	@Autowired
	ResetPasswordFormValid resetValidator;	
	
	@Autowired
	private SessionLocaleResolver localeResolver;

	@Autowired
	private MessageSource messageLocale;	
		
	public void setMessageLocale(MessageSource messageLocale) {
		this.messageLocale = messageLocale;
	}

	public void setValidator(ForgetPasswordFormValid validator) {
		this.validator = validator;
	}

	public void setResetValidator(ResetPasswordFormValid resetValidator) {
		this.resetValidator = resetValidator;
	}	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getforgetpasswordPage(Model model,HttpServletRequest request) {
		
		try{
			initData_Properties(request);
		
		ForgetPasswordForm fpf = new ForgetPasswordForm();
		model.addAttribute("forgetPassword", fpf);

		return "forgetPassword";
		
	}catch(Exception e){
		log.error("getforgetpassword", e);
		throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
	}
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String postforgetpasswordPage(
			@ModelAttribute(value = "forgetPassword") @Valid ForgetPasswordForm fpf,
			BindingResult result, HttpServletRequest request) {
 try{
		if (registrationService.checkIfEmailExists(fpf.getEmail(), "user") != null) {
			String code = BasicService.GetCode();

			ResetPassword resetPassword = new ResetPassword(fpf.getEmail(),
					code, new Date(), "admin");
			resetPasswordService.saveResetPassword(resetPassword);

			try {
				BasicService.SEND_MAIL_MANDRILL(fpf.getEmail(), "", code, "linkForgetPassword",messageLocale,request);				
			} catch (Exception ex) {
				log.error("Error sending email to address:" + fpf.getEmail());
				throw new GenericException("Error, please try later");
			}

		} else {
			validator.validate(fpf, result);
			if (result.hasErrors())
				return "forgetPassword";
		}

		return "redirect:/app/fpp/sendMail";
	}catch(Exception e){
		log.error("postforgetpassword", e);
		throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
	}
	}	

	@RequestMapping(value = "/sendMail", method = RequestMethod.GET)
	public String sentMailPage() {
		return "emailSent";
	}

	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String resetPassword(@RequestParam("code") String code, Model model,
			HttpServletRequest request) {

		try{
		request.getSession().setAttribute("confirmationCode", code);

		ResetPasswordForm rpf = new ResetPasswordForm();
		model.addAttribute("resetPassword", rpf);
		model.addAttribute("carCode", code);

		return "resetPassword";
	}catch(Exception e){
		log.error("resetPassword", e);
		throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
	}
	}

	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public String postResetPassword(
			@ModelAttribute(value = "resetPassword") @Valid ResetPasswordForm rpf,
			BindingResult result, HttpServletRequest request) {

		try{		
			resetValidator.validate(rpf, result);
			if (result.hasErrors()) {				
					return "resetPassword";
		} else {
			try {
				String pass = BasicService.hashPassword(rpf.getNewPassword());
				String confirmationCode = (String) request.getSession()
						.getAttribute("confirmationCode");
				String email = resetPasswordService
						.getEmailByConfirmationCode(confirmationCode);
				if (email == null)
					throw new GenericException(
							"Failed to get email by confirmation code from Database.");
				else {
					User user = userService.getUserByEmail(email);
					user.setPassword(pass);
					userService.save(user);
					resetPasswordService.removeFromResetPassword(email);
				}

			} catch (Exception e) {
				log.error("failed to hash password", e);
				throw new GenericException("Error, Please try later");
			}
		}

		return "redirect:/login.jsp";
	}catch(Exception e){
		log.error("postResetPassword", e);
		throw new GenericException(BasicController.PROP.getProperty("jobApp.genericError"));
	}
	}

}
