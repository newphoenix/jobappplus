package org.phoenix.validation;


import org.phoenix.dto.ResetPasswordForm;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "prototype")
public class ResetPasswordFormValid   implements Validator{

	@Override
	public boolean supports(Class<?> c) {		
		return ResetPasswordForm.class.isAssignableFrom(c);
	}

	@Override
	public void validate(Object command, Errors errors) {
		ResetPasswordForm obj = (ResetPasswordForm) command;
		if(!obj.getNewPassword().equalsIgnoreCase(obj.getConfirmNewPassword()))
			 errors.rejectValue("newPassword","newPassword.notmatch");
	}

}
