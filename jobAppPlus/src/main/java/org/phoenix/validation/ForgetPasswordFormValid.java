package org.phoenix.validation;


import org.phoenix.dto.ForgetPasswordForm;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "prototype")
public class ForgetPasswordFormValid implements Validator {

	@Override
	public boolean supports(Class<?> c) {
		
		return ForgetPasswordForm.class.isAssignableFrom(c);
	}

	@Override
	public void validate(Object command, Errors ers) {
		//ForgetPasswordForm obj = (ForgetPasswordForm) command;
		ers.rejectValue("email","email.doesNotExist"); // this will rejects the email if it d.n.e.
			
		
	}
	
	

}